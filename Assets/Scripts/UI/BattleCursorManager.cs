﻿using UnityEngine;

public class BattleCursorManager : BaseBehaviour
{
	public Texture2D attackCursor;

	private CursorPositionSelector cursorPositionSelector = new CursorPositionSelector();
	private SceneBuilder sceneBuilder;
	private Texture2D lastSetCursor;

	void Awake()
	{
		sceneBuilder = FindObjectOfType<SceneBuilder>();
	}

	void Update()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		IntVector3 insidePosition, outsidePosition;
		bool hasHit = CursorPositionSelector.RaycastToInt(ray, out insidePosition, out outsidePosition);
		if (hasHit)
		{
			DrawUtils1.DrawAABoxFrame(insidePosition);
			GameObject targetObject = sceneBuilder.GetViewObjectAt(insidePosition);
			if (targetObject != null && targetObject.GetComponent<EnemyComponent>() != null)
			{
				Cursor.SetCursor(attackCursor, Vector2.zero, CursorMode.Auto);
				lastSetCursor = attackCursor;
				return;
			}
		}
		if (lastSetCursor != null)
		{
			Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
			lastSetCursor = null;
		}
	}
}
