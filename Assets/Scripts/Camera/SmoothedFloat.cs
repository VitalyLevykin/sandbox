﻿using System;
using UnityEngine;

[Serializable]
public struct SmoothedFloat
{
	public float value;
	public float minValue;
	public float maxValue;
	public float minSoftValue;
	public float maxSoftValue;
	public float smoothSpeed;

	float smoothedValue;

	public SmoothedFloat(float value, float minValue, float maxValue, float minSoftValue, float maxSoftValue, float smoothSpeed)
	{
		this.value = value;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.minSoftValue = minSoftValue;
		this.maxSoftValue = maxSoftValue;
		this.smoothSpeed = smoothSpeed;

		smoothedValue = value;
	}

	public float Value
	{
		get { return value; }
		set { this.value = Mathf.Clamp(value, minValue, maxValue); }
	}

	public float SmoothedValue
	{
		get { return smoothedValue; }
	}

	public float UpdateSmoothedValue(float deltaTime)
	{
		float diff = value - smoothedValue;
		smoothedValue += diff * smoothSpeed * deltaTime;
		return smoothedValue;
	}

	public void ApplySoftLimits()
	{
		value = Mathf.Clamp(value, minSoftValue, maxSoftValue);
	}

	public void ResetSmoothing()
	{
		smoothedValue = value;
	}
}
