﻿using UnityEngine;

public class CameraController : BaseBehaviour
{
	public SmoothedFloat distance = new SmoothedFloat(10, 1, 200, 5, 10, 3);
	public float distanceWheelStep = 1;
	public float distancePinchSpeed = 0.05f;
	public float distanceSoftLimitTimeout = 1;
	public float distanceOnXZMultiplier = 1;
	float distanceChangeTime;
	float? prevPinchDistance = null;

	public SmoothedFloat rotationX = new SmoothedFloat(45, 1, 85, 30, 60, 5);
	public float xRotationPixelToDeg = 0.2f;
	public float xRotationTouchPixelToDeg = 0.001f;

	public bool rotateYAroundScreenCenter;
	public bool rotateYAlongScreenX;
	public float yRotationPixelToDeg = 0.2f;
	public SmoothedFloat rotationY = new SmoothedFloat(45, float.MinValue, float.MaxValue, float.MinValue, float.MaxValue, 10);

	public Vector3 positionOffset = new Vector3(0, 1, 0);
	public float positionSmoothSpeed = 5;
	Transform cameraTarget;
	Vector3? smoothedTarget;

	private bool hasPrevTouch;
	private Touch prevTouch;

	void OnGUI()
	{
		Event e = Event.current;

		if (!InputUtils.IsMouseEvent(e) && !InputUtils.IsTouchEvent())
			return;

		if (InputUtils.IsEmulatedMouseEvent(e))
			return;

		Vector2 clickPosition = InputUtils.IsMouseEvent(e) ? new Vector2(e.mousePosition.x, Screen.height - e.mousePosition.y) : Input.GetTouch(0).position;
		Vector2 deltaPosition = InputUtils.IsMouseEvent(e) ? new Vector2(e.delta.x, -e.delta.y) : (hasPrevTouch ? Input.GetTouch(0).position - prevTouch.position : Vector2.zero);

		// TODO y-up 2d coordinates

		// Move
//		if ((e.type == EventType.MouseDrag && e.button == 0))
//		{
//			Vector2 oldScreenPos = e.mousePosition - e.delta;
//			Vector2 newScreenPos = e.mousePosition;
//
//			IntVector3? position = CursorPositionSelector.GetByGUIPoint(newScreenPos, false, 0, false, Camera.main);
//			float height = position.HasValue ? position.Value.y : 0;
//
//			var horizCameraController = new HorizCameraController(height, 1);
//			Vector3 delta = horizCameraController.CalcMoveDelta(Camera.main, oldScreenPos, newScreenPos);
//
//			transform.position -= delta;
//		}

		// RotateX
		if (/*(e.type == EventType.MouseDrag && e.button == 1) ||*/ (Input.touchCount == 3))
		{
			float deltaY;
			if (InputUtils.IsMouseEvent(e))
			{
				deltaY = e.delta.y * xRotationPixelToDeg;
			}
			else
			{
				float averageTouchSpeedY = (InputUtils.TouchSpeed(0).y + InputUtils.TouchSpeed(1).y + InputUtils.TouchSpeed(2).y) / 3;
				deltaY = -averageTouchSpeedY * xRotationTouchPixelToDeg;
			}
			rotationX.Value += deltaY;
//			rotationX.ResetSmoothing();////
		}
		if (e.type == EventType.MouseUp && e.button == 1)
		{
			rotationX.ApplySoftLimits();
		}
		// RotateY
		if ((e.type == EventType.MouseDrag && e.button == 1) || (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved))
		{
			float deltaAngle = 0;
			if (rotateYAroundScreenCenter)
			{
				deltaAngle = CalcDeltaAngleYAroundScreenCenter(clickPosition, deltaPosition, deltaAngle);
			}
			else if (rotateYAlongScreenX)
			{
				//deltaAngle = -deltaPosition.x * yRotationPixelToDeg;
				deltaAngle = CalcDeltaAngleYAlongScreenX(clickPosition, deltaAngle, deltaPosition);
			}
			else // mixed
			{
				float deltaAngle1 = CalcDeltaAngleYAroundScreenCenter(clickPosition, deltaPosition, deltaAngle);
				float deltaAngle2 = CalcDeltaAngleYAlongScreenX(clickPosition, deltaAngle, deltaPosition);
				if (deltaAngle1 * deltaAngle2 > 0)
				{
					if (deltaAngle1 < 0)
						deltaAngle = Mathf.Min(deltaAngle1, deltaAngle2);
					else
						deltaAngle = Mathf.Max(deltaAngle1, deltaAngle2);
				}
			}
			rotationY.value -= deltaAngle;
		}

		// Zoom
		if (e.type == EventType.ScrollWheel)
		{
			distance.Value += e.delta.y * distanceWheelStep;
			distanceChangeTime = Time.time;
		}
		// Pinch to zoom
		if (Input.touchCount == 2)
		{
			Vector2 touch0 = Input.GetTouch(0).position;
			Vector2 touch1 = Input.GetTouch(1).position;

			float pinchDistance = Vector2.Distance(touch0, touch1);

			if (!prevPinchDistance.HasValue)
				prevPinchDistance = pinchDistance;

			float distanceDiff = pinchDistance - prevPinchDistance.Value;
			distance.Value -= distanceDiff * distancePinchSpeed;

			prevPinchDistance = pinchDistance;
		}
		else
		{
			prevPinchDistance = null;
		}

		if (Input.touchCount > 0)
		{
			hasPrevTouch = true;
			prevTouch = Input.GetTouch(0);
		}
	}

	private float CalcDeltaAngleYAlongScreenX(Vector2 clickPosition, float deltaAngle, Vector2 deltaPosition)
	{
		Vector2 centerPosition = new Vector2((float) Screen.width / 2, (float) Screen.height / 2);
		float deadZoneFactor = Mathf.Clamp01(Mathf.Abs(clickPosition.y - centerPosition.y) / 50);
		deltaAngle = -deltaPosition.x * yRotationPixelToDeg;
		deltaAngle *= deadZoneFactor;
		if (clickPosition.y > centerPosition.y)
			deltaAngle = -deltaAngle;
		return deltaAngle;
	}

	private float CalcDeltaAngleYAroundScreenCenter(Vector2 clickPosition, Vector2 deltaPosition, float deltaAngle)
	{
		Vector2 centerPosition = new Vector2((float) Screen.width / 2, (float) Screen.height / 2);
		Vector2 moveOut = GetMoveOutIfTooClose(clickPosition, centerPosition, 50);
		clickPosition += moveOut;

		Vector2 prevClickPosition = clickPosition - deltaPosition;

		float anglePrev = Angle(prevClickPosition - centerPosition);
		float angle = Angle(clickPosition - centerPosition);
		deltaAngle = angle - anglePrev;
		deltaAngle = RemoveFullRotations(deltaAngle);
		return deltaAngle;
	}

	private float RemoveFullRotations(float angle)
	{
		if (angle >= 180)
			angle -= 360;
		if (angle < -180)
			angle += 360;
		return angle;
	}

	private float Angle(Vector2 to)
	{
		float dot = Vector2.Dot(Vector2.up, to.normalized);
		float angle = Mathf.Acos(dot) * Mathf.Rad2Deg;
		return to.x >= 0 ? angle : 360 - angle;
	}

	private Vector2 GetMoveOutIfTooClose(Vector2 point, Vector2 center, float minDistance)
	{
		Vector2 radiusVector = point - center;
		if (Vector2.SqrMagnitude(radiusVector) >= minDistance * minDistance)
			return Vector2.zero;
		return radiusVector.normalized * minDistance + center - point;
	}

	void Update()
	{
		if (cameraTarget == null && (cameraTarget = FindCameraTarget()) == null)
			return;

		if (Time.time > distanceChangeTime + distanceSoftLimitTimeout)
			distance.ApplySoftLimits();

		ApplySmoothing();

		CalculateTransform();
	}

	Transform FindCameraTarget()
	{
		var person = FindObjectOfType<SimpleMove>();
		return person != null ? person.transform : null;
	}

	void ApplySmoothing()
	{
		if (!smoothedTarget.HasValue)
			smoothedTarget = cameraTarget.position;

		Vector3 targetPositionDiff = cameraTarget.position - smoothedTarget.Value;
		smoothedTarget += targetPositionDiff * Time.deltaTime * positionSmoothSpeed;

		rotationX.UpdateSmoothedValue(Time.deltaTime);
		rotationY.UpdateSmoothedValue(Time.deltaTime);
		distance.UpdateSmoothedValue(Time.deltaTime);
	}

	void CalculateTransform()
	{
		float sinX = Mathf.Sin(rotationX.SmoothedValue * Mathf.Deg2Rad);
		float cosX = Mathf.Cos(rotationX.SmoothedValue * Mathf.Deg2Rad);
		float sinY = Mathf.Sin(rotationY.SmoothedValue * Mathf.Deg2Rad);
		float cosY = Mathf.Cos(rotationY.SmoothedValue * Mathf.Deg2Rad);

		float distanceOnXZ = distance.SmoothedValue * cosX;
		float y = distance.SmoothedValue * sinX;

		float x = -distanceOnXZ * sinY;
		float z = -distanceOnXZ * cosY;

		Vector3 target = smoothedTarget.Value + positionOffset;
		transform.position = target + new Vector3(x, y, z);
		transform.LookAt(target);


		distanceOnXZ = distance.SmoothedValue * cosX * distanceOnXZMultiplier;
		x = -distanceOnXZ * sinY;
		z = -distanceOnXZ * cosY;
		transform.position = target + new Vector3(x, y, z);
	}
}
