using UnityEngine;

public class HorizCameraController
{
	private readonly int _heightAxis;
	private readonly float _terrainHeightOnEmptyDrag;
	
	public HorizCameraController(float terrainHeightOnEmptyDrag, int heightAxis)
	{
		_terrainHeightOnEmptyDrag = terrainHeightOnEmptyDrag;
		_heightAxis = heightAxis;
	}
	
	public Vector3 CalcMoveDelta(Camera camera, Vector2 screenPointFrom, Vector2 screenPointTo)
	{
		//Contract.Requires/*<ArgumentNullException>*/(camera != null);

		Ray rayFrom = GeometryUtils.GUIPointToWorldRay(screenPointFrom, camera);
		Ray rayTo = GeometryUtils.GUIPointToWorldRay(screenPointTo, camera);
		
		float height = CalcHeightFromRayHit(rayFrom);
		Plane heightPlane = CreateHeightPlane(height);
		
		Vector3 worldFrom = PlaneRayIntersection(heightPlane, rayFrom);
		Vector3 worldTo = PlaneRayIntersection(heightPlane, rayTo);
		Vector3 delta = worldTo - worldFrom;

		return delta;
	}
	
	private static Vector3 PlaneRayIntersection(Plane plane, Ray ray)
	{
		float rayToIntersectDistance;
		plane.Raycast(ray, out rayToIntersectDistance);
		
		return ray.GetPoint(rayToIntersectDistance);
	}
	
	private Plane CreateHeightPlane(float height)
	{
		var planeNormal = new Vector3();
		planeNormal[_heightAxis] = 1;
		return new Plane(planeNormal, planeNormal * height);
	}
	
	private float CalcHeightFromRayHit(Ray ray)
	{
		RaycastHit hit;
		
		float height = _terrainHeightOnEmptyDrag;
		if (Physics.Raycast(ray, out hit))
		{
			height = hit.point[_heightAxis];
		}
		return height;
	}
}
