using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions.Must;

[Serializable]
public class VerticalList<T, TChunk> : ISerializationCallbackReceiver where TChunk : VerticalListChunk<T>, new()
{
//	[SerializeField]
	private LinkedList<TChunk> chunks = new LinkedList<TChunk>();

	[SerializeField]
	private List<TChunk> chunksSer = new List<TChunk>();

//	[NonSerialized]
	[SerializeField]
	/*private*/public int x, z;

	//! OnAdd (append; new chunk; merge)
	//! OnReplace (only; reduce+new; reduce+append; split)
	//! OnClear (reduce; split)
	
	public delegate void OnAddDelegate(IntVector3 position, int prevTop, int prevBottom, TChunk chunk);
	public delegate void OnAddNewDelegate(IntVector3 position, TChunk chunk);
//	public delegate void OnRemoveDelegate(IntVector3 position, int prevTop, int prevBottom, TChunk chunk);
//	public delegate void OnInstantiateChunkDelegate(TChunk chunk, int x, int z);
	public delegate void OnClearDelegate(IntVector3 position, int prevTop, int prevBottom, TChunk chunk, TChunk newLowerChunk);
	public delegate void OnMergeDelegate(IntVector3 position, TChunk chunk, TChunk nextChunk);
	public delegate void OnAfterChangeDelegate(IntVector3 position, TChunk chunk);

	public OnAddDelegate onAdd;
	public OnAddNewDelegate onAddNew;
//	public OnRemoveDelegate onRemove;
//	public OnInstantiateChunkDelegate onInstantiateChunk;
	public OnClearDelegate onClear;
	public OnMergeDelegate onMerge;
	public OnAfterChangeDelegate onAfterChange;

	private int bottom = int.MaxValue;
	private int top = int.MinValue;

	public void OnBeforeSerialize()
	{
		chunksSer.Clear();
		foreach (var chunk in chunks)
		{
			var chunkSer = new TChunk();
			chunkSer.Init(chunk.Element, chunk.Top, chunk.Length, false);
			chunksSer.Add(chunkSer);
		}
	}

	public void OnAfterDeserialize()
	{
		chunks.Clear();
		foreach (var chunk in chunksSer)
		{
			chunks.AddLast((TChunk)new TChunk().Init(chunk.Element, chunk.Top, chunk.Length, false));
		}
		UpdateBounds();
	}

	public void SetXZ(int x, int z)
	{
		this.x = x;
		this.z = z;
	}

	public T Get(int index)
	{
		if (!IsEmpty())
		{
			foreach (var chunk in chunks)
			{
				if (index > chunk.Top)
					return default(T);
				if (index >= chunk.Bottom)
					return chunk.Element;
			}
		}
		return default(T);
	}

	public T GetStrict(int index)
	{
		var element = Get(index);
		if (VerticalListChunk<T>.IsElementDefault(element))
			throw new ArgumentOutOfRangeException("index=" + index);
		return element;
	}
	
	public bool Has(int index)
	{
		var element = Get(index);
		return !VerticalListChunk<T>.IsElementDefault(element);
	}
	
	public bool IsEmpty()
	{
		return chunks == null || chunks.Count == 0;
	}

	public T Set(int index, T element)
	{
		if (VerticalListChunk<T>.IsElementDefault(element))
			throw new ArgumentNullException("element");

		// TODO UpdateBounds before each return

		if (IsEmpty())
		{
			chunks = new LinkedList<TChunk>();
			var newChunk = (TChunk)new TChunk().Init(element, index, 1);
			chunks.AddLast(newChunk);
			//! OnAdd new
			OnAddNew(index, newChunk);
			return default(T);
		}

		int i = 0;

		for (LinkedListNode<TChunk> chunkNode = chunks.First; chunkNode != null; chunkNode = chunkNode.Next)
		{
			TChunk chunk = chunkNode.Value;
			if (index > chunk.Top + 1) // higher
			{
				var newChunk = (TChunk)new TChunk().Init(element, index, 1);
				chunks.AddBefore(chunkNode, newChunk);
				//! OnAdd new
				OnAddNew(index, newChunk);
				return default(T);
			}
			else if (index == chunk.Top + 1) // just higher
			{
				if (chunk.ElementEquals(element))
				{
					chunk.Top++;
					//! OnAdd append top
					OnAddAppend(index, chunk.Top - 1, chunk.Bottom, chunk);
				}
				else
				{
					var newChunk = (TChunk)new TChunk().Init(element, index, 1);
					chunks.AddBefore(chunkNode, newChunk);
					//! OnAdd new
					OnAddNew(index, newChunk);
				}
				return default(T);
			}
			else if (index >= chunk.Bottom) // inside
			{
				T prev = chunk.Element;
				if (index == chunk.Top) // near top; upper is clear
				{
					if (chunk.ElementEquals(element))
					{
						// nothing
					}
					else
					{
						if (chunk.Length == 1)
						{
							T prevElement = chunk.Element;
							chunk.Element = element;
							//! OnReplace only
							OnReplaceOnly(index, chunkNode, prevElement);
						}
						else
						{
							var newChunk = (TChunk)new TChunk().Init(element, index, 1);
							chunks.AddBefore(chunkNode, newChunk);
							int prevTop = chunk.Top;
							int prevBottom = chunk.Bottom;
							chunk.Top--;
							//! OnReplace reduce+new
							OnReplaceReduceNew(index, prevTop, prevBottom, chunk, newChunk);
						}
					}
				}
				else if (index == chunk.Bottom) // near bottom; length > 1
				{
					if (chunk.ElementEquals(element))
					{
						// nothing
					}
					else
					{
						TChunk nextChunk = chunkNode.Next != null ? chunkNode.Next.Value : null;
						bool increaseNext = nextChunk != null && (nextChunk.Top == index - 1) && nextChunk.ElementEquals(element);
						int prevBottom = chunk.Bottom;
						chunk.Bottom++; 
						if (increaseNext)
						{
							int prevNextTop = nextChunk.Top;
							nextChunk.Top = index;
							//! OnReplace reduce+append
							OnReplaceReduceAppend(index, chunk, prevNextTop, nextChunk);
						}
						else
						{
							var newChunk = (TChunk)new TChunk().Init(element, index, 1);
							chunks.AddAfter(chunkNode, newChunk);
							//! OnReplace reduce+new
							OnReplaceReduceNew(index, chunk.Top, prevBottom, chunk, newChunk);
						}
					}
				}
				else // inside
				{
					if (chunk.ElementEquals(element))
					{
						// nothing
					}
					else
					{
						// split
						int bottom = chunk.Bottom;
						chunk.Bottom = index + 1;
						var newChunk = (TChunk)new TChunk().Init(element, index, 1);
						LinkedListNode<TChunk> newChunkNode = chunks.AddAfter(chunkNode, newChunk);

						TChunk bottomChunk = CloneChunk(chunk, index - 1, bottom);
						chunks.AddAfter(newChunkNode, bottomChunk);
						//! OnReplace split
						OnSplit(index, chunk, newChunk, bottomChunk);
					}
				}
				return prev;
			}
			else if (index == chunk.Bottom - 1) // just lower
			{
				/*
				+1. at next
				 +1. same as lower - nothing
				 +2. at next, next is == 1
				  +1. no nextNext
				   +1. equals upper - AppendReplaceOnly
				   +2. ReplaceOnly
				  +2. has nextNext
				   +all combos (4):
				   +bool newEqualsCurrent
				   +bool newEqualsNextNext
				 +3. at next, next is > 1
				  +1. equals upper - ReplaceAppendReduce
				  +2. different to both - ReplaceReduceNew
				+2. gap == 1
				 +all combos (4):
				 +1. append
				 +2. append lower
				 +3. AddNew
				 +4. merge
				+3. gap > 1 (next != null)
				 +1. append
				 +2. AddNew
				 */


				// gather info
				T prev = default(T);
				bool equalsCurrent = chunk.ElementEquals(element);
				TChunk nextChunk = null;
				bool equalsNext = false;
				LinkedListNode<TChunk> nextChunkNode = chunkNode.Next;
				if (nextChunkNode != null)
				{
					nextChunk = nextChunkNode.Value;
					equalsNext = nextChunk.ElementEquals(element);
					if (nextChunk.Top == index)
						prev = nextChunk.Element;
				}

				// at next chunk?
				if (nextChunk != null && nextChunk.Top == index)
				{
					if (equalsNext)
					{
						// do nothing
					}
					else
					{
						if (nextChunk.Length == 1)
						{
							TChunk nextNextChunk = null;
							if (nextChunkNode.Next != null && nextChunkNode.Next.Value.Top + 1 == index)
								nextNextChunk = nextChunkNode.Next.Value;

							if (nextNextChunk != null)
							{
								bool equalsNextNext = nextNextChunk.ElementEquals(element);

								if (equalsCurrent && equalsNextNext)
								{
									chunks.Remove(nextChunkNode.Next);
									chunks.Remove(nextChunkNode);
									chunk.Bottom = nextNextChunk.Bottom;
									//! MergeAllReplace
									OnReplaceMerge(index, chunk, nextChunk, nextNextChunk);
								}
								else if (equalsCurrent && !equalsNextNext)
								{
									int prevBottom = chunk.Bottom;
									chunk.Bottom--;
									chunks.Remove(nextChunkNode);
									//! AppendReplaceOnly
									OnAppendReplaceOnly(index, chunk.Top, prevBottom, chunk, nextChunk);
								}
								else if (!equalsCurrent && equalsNextNext)
								{
									int prevTop = nextNextChunk.Top;
									nextNextChunk.Top++;
									chunks.Remove(nextChunkNode);
									//! ReplaceOnlyAppend
									OnAppendReplaceOnly(index, prevTop, nextNextChunk.Bottom, nextNextChunk, nextChunk);
								}
								else // !equalsCurrent && !equalsNextNext
								{
									T prevElement = nextChunk.Element;
									nextChunk.Element = element;
									//! ReplaceOnly
									OnReplaceOnly(index, nextChunkNode, prevElement);
								}
							}
							else
							{
								if (equalsCurrent)
								{
									int prevBottom = chunk.Bottom;
									chunk.Bottom--;
									chunks.Remove(nextChunkNode);
									//! AppendReplaceOnly
									OnAppendReplaceOnly(index, chunk.Top, prevBottom, chunk, nextChunk);
								}
								else
								{
									T prevElement = nextChunk.Element;
									nextChunk.Element = element;
									//! ReplaceOnly
									OnReplaceOnly(index, nextChunkNode, prevElement);
								}
							}
						}
						else
						{
							if (equalsCurrent)
							{
								int prevBottom = chunk.Bottom;
								int prevNextTop = nextChunk.Top;
								chunk.Bottom--;
								nextChunk.Top--;
								//! ReplaceAppendReduce
								OnReplaceAppendReduce(index, prevBottom, chunk, prevNextTop, nextChunk);
							}
							else
							{
								int prevNextTop = nextChunk.Top;
								nextChunk.Top--;
								var newChunk = (TChunk)new TChunk().Init(element, index, 1);
								LinkedListNode<TChunk> newChunkNode = chunks.AddAfter(chunkNode, newChunk);
								//! ReplaceReduceNextNew
								OnReplaceReduceNew(index, prevNextTop, nextChunk.Bottom, nextChunk, newChunk);
							}
						}
					}
				}
				// gap == 1
				else if (nextChunk != null && nextChunk.Top + 1 == index)
				{
					if (equalsCurrent && equalsNext)
					{
						chunks.Remove(nextChunkNode);
						chunk.Bottom = nextChunk.Bottom;
						//! MergeAll
						OnMerge(index, chunk, nextChunk);
					}
					else if (equalsCurrent && !equalsNext)
					{
						int prevBottom = chunk.Bottom;
						chunk.Bottom--;
						//! Append
						OnAddAppend(index, chunk.Top, prevBottom, chunk);
					}
					else if (!equalsCurrent && equalsNext)
					{
						int prevTop = nextChunk.Top;
						nextChunk.Top++;
						//! Append lower
						OnAddAppend(index, prevTop, nextChunk.Bottom, nextChunk);
					}
					else // !equalsCurrent && !equalsNext
					{
						var newChunk = (TChunk)new TChunk().Init(element, index, 1);
						chunks.AddAfter(chunkNode, newChunk);
						//! OnAdd new
						OnAddNew(index, newChunk);
					}
				}
				// gap > 1?
				else
				{
					if (equalsCurrent)
					{
						int prevBottom = chunk.Bottom;
						chunk.Bottom--;
						//! Append
						OnAddAppend(index, chunk.Top, prevBottom, chunk);
					}
					else
					{
						var newChunk = (TChunk)new TChunk().Init(element, index, 1);
						chunks.AddAfter(chunkNode, newChunk);
						//! OnAdd new
						OnAddNew(index, newChunk);
					}
				}

				return prev;
			}
			else if (chunkNode.Next == null) // lower all
			{
				var newChunk = (TChunk)new TChunk().Init(element, index, 1);
				chunks.AddAfter(chunkNode, newChunk);
				//! OnAdd new
				OnAddNew(index, newChunk);
				return default(T);
			}

			i++;
		}

		throw new NotImplementedException();
	}

	public int GetTop()
	{
		return top;
	}

	public int GetBottom()
	{
		return bottom;
	}

	private void OnAddNew(int index, TChunk newChunk)
	{
		if (onAddNew != null)
		{
			var position = new IntVector3(x, index, z);
			onAddNew(position, newChunk);
		}

		OnAfterChange(index, newChunk);
	}

	private void OnAddAppend(int index, int prevTop, int prevBottom, TChunk chunk)
	{
		if (onAdd != null)
		{
			var position = new IntVector3(x, index, z);
			onAdd(position, prevTop, prevBottom, chunk);
		}

		OnAfterChange(index, chunk);
	}

	private void OnReplaceOnly(int index, LinkedListNode<TChunk> chunkNode, T prevElement)
	{
		var position = new IntVector3(x, index, z);
		TChunk chunk = chunkNode.Value;

		// hack - place element back so that listeners received old state
		// TODO inline calling code here or re-create element instead of set chunk.Element

//		// removing from list, so that onClear could not see this chunk
		T element = chunk.Element;
		chunk.Element = prevElement;
//		LinkedListNode<TChunk> prevNode = chunkNode.Previous;
//		chunks.Remove(chunkNode);

		if (onClear != null)
			onClear(position, index, index, chunk, null);

//		// adding chunk back
		chunk.Element = element;
//		if (prevNode != null)
//			chunks.AddAfter(prevNode, chunkNode);
//		else
//			chunks.AddFirst(chunkNode);

		if (onAddNew != null)
			onAddNew(position, chunk);

		OnAfterChange(index, chunk);
	}

	private void OnReplaceReduceNew(int index, int prevTop, int prevBottom, TChunk prevChunk, TChunk newChunk)
	{
		var position = new IntVector3(x, index, z);
		if (onClear != null)
			onClear(position, prevTop, prevBottom, prevChunk, null);
		if (onAddNew != null)
			onAddNew(position, newChunk);

		OnAfterChange(index, newChunk);
	}

	private void OnAppendReplaceOnly(int index, int prevTop, int prevBottom, TChunk chunk, TChunk removedChunk)
	{
		var position = new IntVector3(x, index, z);
		if (onClear != null)
			onClear(position, index, index, removedChunk, null);
		if (onAdd != null)
			onAdd(position, prevTop, prevBottom, chunk);

		OnAfterChange(index, chunk);
	}

	private void OnReplaceAppendReduce(int index, int prevBottom, TChunk chunk, int prevNextTop, TChunk nextChunk)
	{
		var position = new IntVector3(x, index, z);
		int prevTop = chunk.Top;

		if (onClear != null)
			onClear(position, prevNextTop, nextChunk.Bottom, nextChunk, null);
		if (onAdd != null)
			onAdd(position, prevTop, prevBottom, chunk);

		OnAfterChange(index, chunk);
	}

	private void OnReplaceReduceAppend(int index, TChunk chunk, int prevNextTop, TChunk nextChunk)
	{
		var position = new IntVector3(x, index, z);
		prevNextTop.MustBeEqual(index - 1);

		if (onClear != null)
			onClear(position, chunk.Top, index, chunk, null);
		if (onAdd != null)
			onAdd(position, prevNextTop, nextChunk.Bottom, nextChunk);

		OnAfterChange(index, chunk);
	}

	private void OnReplaceMerge(int index, TChunk chunk, TChunk nextChunk, TChunk nextNextChunk)
	{
		var position = new IntVector3(x, index, z);

		if (onClear != null)
			onClear(position, index, index, nextChunk, null);
		if (onMerge != null)
			onMerge(position, chunk, nextNextChunk);

		OnAfterChange(index, chunk);
	}

	private void OnMerge(int index, TChunk chunk, TChunk nextChunk)
	{
		var position = new IntVector3(x, index, z);

		if (onMerge != null)
			onMerge(position, chunk, nextChunk);

		OnAfterChange(index, chunk);
	}

	private void OnSplit(int index, TChunk prevChunk, TChunk chunk, TChunk nextChunk)
	{
		var position = new IntVector3(x, index, z);
		int prevTop = prevChunk.Top;
		int prevBottom = nextChunk.Bottom;

		if (onClear != null)
			onClear(position, prevTop, prevBottom, prevChunk, nextChunk);
		if (onAddNew != null)
			onAddNew(position, chunk);

		OnAfterChange(index, chunk);
	}

	private void OnAfterChange(int index, TChunk newChunk)
	{
		var position = new IntVector3(x, index, z);

		if (onAfterChange != null)
			onAfterChange(position, newChunk);
	}

	private TChunk CloneChunk(TChunk chunk, int top, int bottom)
	{
		T elementClone = CloneElement(chunk.Element);
		var clone = (TChunk)new TChunk().Init(elementClone, top, 1);
		clone.Bottom = bottom;
		return clone;
	}

	private T CloneElement(T element)
	{
		if (element is ICloneable)
		{
			return (T) ((ICloneable) element).Clone();
		}
		return element;
	}

	public T ClearElement(int index)
	{
		if (IsEmpty())
		{
			return default(T);
		}

		LinkedListNode<TChunk> chunkNode = GetNode(index);
		if (chunkNode == null)
		{
			return default(T);
		}

		TChunk chunk = chunkNode.Value;
		TChunk newLowerChunk = null;
		int prevTop = chunk.Top;
		int prevBottom = chunk.Bottom;

		if (index == chunk.Top && index == chunk.Bottom)
		{
			chunks.Remove(chunkNode);
		}
		else if (index == chunk.Top)
		{
			chunk.Top--;
		}
		else if (index == chunk.Bottom)
		{
			chunk.Bottom++;
		}
		else
		{
			int bottom = chunk.Bottom;
			newLowerChunk = CloneChunk(chunk, index - 1, bottom);
			chunk.Bottom = index + 1;
			chunks.AddAfter(chunkNode, newLowerChunk);
		}

		UpdateBounds();

		if (onClear != null)
			onClear(new IntVector3(x, index, z), prevTop, prevBottom, chunk, newLowerChunk);

		OnAfterChange(index, chunk);

		return chunk.Element;
	}

	public void Clear()
	{
		chunks = null;
		// TODO callbacks
	}

	public IntVector3 GetPosition(int y)
	{
		return new IntVector3(x, y, z);
	}

	public LinkedListNode<TChunk> GetNode(int index)
	{
		if (IsEmpty())
			return null;

		for (LinkedListNode<TChunk> chunkNode = chunks.First; chunkNode != null; chunkNode = chunkNode.Next)
		{
			TChunk chunk = chunkNode.Value;
			if (index > chunk.Top)
			{
				return null;
			}
			if (index >= chunk.Bottom)
			{
				return chunkNode;
			}
		}
		return null;
	}

	internal LinkedList<TChunk> GetChunks()
	{
		return chunks;
	}

	private void UpdateBounds()
	{
		bottom = int.MaxValue;
		top = int.MinValue;
		if (chunks.First != null)
		{
			ExpandBounds(chunks.First.Value.Top);
			ExpandBounds(chunks.Last.Value.Bottom);
		}
	}

	private void ExpandBounds(int position)
	{
		if (bottom > position)
			bottom = position;
		if (top < position)
			top = position;
	}

	public override string ToString()
	{
		if (IsEmpty())
			return "[EmptyVerticalList(" + x + "," + z + ")]";
		string[] chunkDumps = chunks.Select((chunk) => { return chunk.ToString(); }).ToArray();
		return "[VerticalList(" + x + "," + z + ") Chunks: " + String.Join(", ", chunkDumps) + "]";
	}

	public void Dump()
	{
		Console.WriteLine(ToString());
	}
}
