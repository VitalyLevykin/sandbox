﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ArrayOfVerticalListsVoxelStorage<T, TList, TChunk> : IVoxelStorage<T, TList, TChunk>, ISerializationCallbackReceiver
	where TList : VerticalList<T, TChunk>, new()
	where TChunk : VerticalListChunk<T>, new()
{
	public const int MAX_SIZE = 5;

	public IntVector3 GetMinPosition()
	{
		return new IntVector3(0, 0, 0);
	}

	public IntVector3 GetMaxPosition()
	{
		return new IntVector3(MAX_SIZE - 1, MAX_SIZE - 1, MAX_SIZE - 1);
	}

	public T Get(IntVector3 position)
	{
		TList verticalList = GetVerticalList(position.x, position.z);
		if (verticalList != null)
		{
			return verticalList.Get(position.y);
		}
		return default(T);
	}

	public bool Has(IntVector3 position)
	{
		return Get(position) != null;
	}

	public void Set(IntVector3 position, T value)
	{
		GetOrCreateVerticalList(position.x, position.z).Set(position.y, value);
	}

	public void ClearElement(IntVector3 position)
	{
		TList verticalList = GetVerticalList(position.x, position.z);
		if (verticalList != null)
		{
			verticalList.ClearElement(position.y);
			if (verticalList.IsEmpty())
			{
				// TODO not set null and not check for null in GetOrCreateVerticalList?
				RemoveListenerFromVerticalList(verticalList);
				SetVerticalList(position.x, position.z, null);
			}
		}
	}

	public TList GetVerticalList(int x, int z)
	{
		return ar[x * MAX_SIZE + z];
	}

	public LinkedListNode<TChunk> GetVerticalListNode(IntVector3 position)
	{
		return null;
	}

	private void SetVerticalList(int x, int z, TList verticalList)
	{
		ar[x * MAX_SIZE + z] = verticalList;
	}

	private TList GetOrCreateVerticalList(int x, int z)
	{
		TList verticalList = GetVerticalList(x, z);
		// vertiacal list should not be null when empty, but SOMEHOW it can (serialization?)
		if (verticalList == null || verticalList.IsEmpty())
		{
			verticalList = new TList();
			AddListenerToVerticalList(verticalList, x, z);

			SetVerticalList(x, z, verticalList);
		}
		return verticalList;
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return GetEnumerator();
	}

	public IEnumerator<TList> GetEnumerator()
	{
		for (int x = 0; x < MAX_SIZE; x++)
		{
			for (int z = 0; z < MAX_SIZE; z++)
			{
				TList verticalList = GetVerticalList(x, z);
				if (verticalList != null)
					yield return verticalList;
			}
		}
	}

	public string Dump()
	{
		string dump = "Dump:\n";
		for (int x = 0; x < MAX_SIZE; x++)
		{
			for (int z = 0; z < MAX_SIZE; z++)
			{
				for (int y = 0; y < MAX_SIZE; y++)
				{
					T v = Get(new IntVector3(x, y, z));
					if (!VerticalListChunk<T>.IsElementDefault(v))
					{
						dump += " (" + x + ";" + y + ";" + z + ")=" + v + "\n";
					}
				}
			}
		}
		return dump;
	}

	///////////// LISTENING

	public event VerticalList<T, TChunk>.OnAddDelegate OnAdd;
	public event VerticalList<T, TChunk>.OnAddNewDelegate OnAddNew;
	//	public event VerticalList<T, TChunk>.OnRemoveDelegate OnRemove;
//	public event VerticalList<T, TChunk>.OnInstantiateChunkDelegate OnInstantiateChunk;
	public event VerticalList<T, TChunk>.OnClearDelegate OnClear;
	public event VerticalList<T, TChunk>.OnMergeDelegate OnMerge;
	public event VerticalList<T, TChunk>.OnAfterChangeDelegate OnAfterChange;

	[SerializeField]
	private TList[] ar = new TList[MAX_SIZE * MAX_SIZE];

	public void OnBeforeSerialize()
	{
		AddListenerToAllLists();
	}

	public void OnAfterDeserialize()
	{
		AddListenerToAllLists();
	}

	private void AddListenerToAllLists()
	{
		for (int x = 0; x < MAX_SIZE; x++)
		{
			for (int z = 0; z < MAX_SIZE; z++)
			{
				TList verticalList = GetVerticalList(x, z);
				if (verticalList != null)
				{
					AddListenerToVerticalList(verticalList, x, z);
				}
			}
		}
	}

	private void AddListenerToVerticalList(TList verticalList, int x, int z)
	{
		verticalList.SetXZ(x, z);
		if (verticalList.onAdd == null) // TODO without check, multiple add possible
			verticalList.onAdd += OnAddInVerticalList;
		if (verticalList.onAddNew == null)
			verticalList.onAddNew += OnAddNewInVerticalList;
		//		verticalList.onRemove += OnRemoveInVerticalList;
//		if (verticalList.onInstantiateChunk == null)
//			verticalList.onInstantiateChunk += OnInstantiateChunkInVerticalList;
		if (verticalList.onClear == null)
			verticalList.onClear += OnClearInVerticalList;
		if (verticalList.onMerge == null)
			verticalList.onMerge += OnMergeInVerticalList;
		if (verticalList.onAfterChange == null)
			verticalList.onAfterChange += OnAfterChangeInVerticalList;
	}

	private void RemoveListenerFromVerticalList(TList verticalList)
	{
		verticalList.onAdd -= OnAddInVerticalList;
		verticalList.onAddNew -= OnAddNewInVerticalList;
		//		verticalList.onRemove -= OnRemoveInVerticalList;
//		verticalList.onInstantiateChunk -= OnInstantiateChunkInVerticalList;
		verticalList.onClear -= OnClearInVerticalList;
		verticalList.onMerge -= OnMergeInVerticalList;
		verticalList.onAfterChange -= OnAfterChangeInVerticalList;
	}

	private void OnAddInVerticalList(IntVector3 position, int prevTop, int prevBottom, TChunk chunk)
	{
		OnAdd(position, prevTop, prevBottom, chunk);
	}

	private void OnAddNewInVerticalList(IntVector3 position, TChunk chunk)
	{
		OnAddNew(position, chunk);
	}

	//	private void OnRemoveInVerticalList(IntVector3 position, int prevTop, int prevBottom, TChunk chunk)
	//	{
	//		OnRemove(position, prevTop, prevBottom, chunk);
	//	}

//	private void OnInstantiateChunkInVerticalList(TChunk chunk, int x, int z)
//	{
//		OnInstantiateChunk(chunk, x, z);
//	}

	private void OnClearInVerticalList(IntVector3 position, int prevTop, int prevBottom, TChunk chunk, TChunk newLowerChunk)
	{
		OnClear(position, prevTop, prevBottom, chunk, newLowerChunk);
	}

	private void OnMergeInVerticalList(IntVector3 position, TChunk chunk, TChunk nextChunk)
	{
		OnMerge(position, chunk, nextChunk);
	}

	private void OnAfterChangeInVerticalList(IntVector3 position, TChunk chunk)
	{
		OnAfterChange(position, chunk);
	}

	public IntVector3 BoundsMin { get { return new IntVector3();} }
	public IntVector3 BoundsMax { get { return new IntVector3(MAX_SIZE, 0, MAX_SIZE); } }
}
