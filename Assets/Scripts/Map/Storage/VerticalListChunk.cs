using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class VerticalListChunk<T> : ISerializationCallbackReceiver
{
	[SerializeField]
	T element;
	[SerializeField]
	int top;
	[SerializeField]
	int bottom;

	public void OnBeforeSerialize()
	{
		//
	}

	public void OnAfterDeserialize()
	{
//		Debug.Log("OnAfterDeserialize2 " + top);
	}

	public VerticalListChunk()
	{
	}

//	public VerticalListChunk(T element, int top, int length)
//	{
//		Init(element, top, length, true);
//	}
//
//	public VerticalListChunk(T element, int top, int length, bool check)
//	{
//		Init(element, top, length, check);
//	}

	public VerticalListChunk<T> Init(T element, int top, int length, bool check = true)
	{
		this.element = element;
		this.top = top;
		this.bottom = top - length + 1;

		if (check)
			CheckState();
		return this;
	}

	public T Element
	{
		get
		{
			return element;
		}
		set { element = value; }
	}

	public int Top
	{
		get
		{
			return top;
		}
		set
		{
			if (value < bottom)
				throw new ArgumentException();
			top = value;
		}
	}
	
	public int Length
	{
		get
		{
			return top - bottom + 1;
		}
	}

	public int Bottom
	{
		get
		{
			return bottom;
		}
		set
		{
			if (value > top)
				throw new ArgumentException();
			bottom = value;
		}
	}

	public void CheckState()
	{
		if (ElementEquals(default(T)))
			throw new ArgumentException ("element");
		if (Length <= 0)
			throw new ArgumentException("length");
	}

	public bool ElementEquals(T other)
	{
		return EqualityComparer<T>.Default.Equals(element, other);
	}

	public static bool IsElementDefault(T element)
	{
		return ElementsEqual(element, default(T));
	}

	public static bool ElementsEqual(T element1, T element2)
	{
		return EqualityComparer<T>.Default.Equals(element1, element2);
	}
	
	public override string ToString()
	{
		return string.Format ("[element={0};top={1};length={2}]", element, top, Length);
	}
}
