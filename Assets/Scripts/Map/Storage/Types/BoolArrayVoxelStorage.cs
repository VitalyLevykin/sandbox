﻿using System;

[Serializable]
public class BoolVerticalListChunk : VerticalListChunk<bool> { }

[Serializable]
public class BoolVerticalList : VerticalList<bool, BoolVerticalListChunk> { }

//[Serializable]
//public class BoolArrayVoxelStorage : ArrayVoxelStorage<bool> { }
//
//[Serializable]
//public class BoolArrayOfVerticalListsVoxelStorage : ArrayOfVerticalListsVoxelStorage<bool> { }
