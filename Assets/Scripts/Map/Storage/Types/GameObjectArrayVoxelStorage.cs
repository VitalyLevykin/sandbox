﻿using System;
using UnityEngine;

[Serializable]
public class GameObjectVerticalListChunk : VerticalListChunk<GameObject> { }

[Serializable]
public class GameObjectVerticalList : VerticalList<GameObject, GameObjectVerticalListChunk> { }

//[Serializable]
//public class GameObjectArrayVoxelStorage : ArrayVoxelStorage<GameObject> { }

[Serializable]
public class GameObjectArrayOfVerticalListsVoxelStorage : ArrayOfVerticalListsVoxelStorage<GameObject, GameObjectVerticalList, GameObjectVerticalListChunk> { }
