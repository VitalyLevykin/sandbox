﻿using System;

[Serializable]
public class VoxelVerticalListChunk : VerticalListChunk<MapVoxel> { }

[Serializable]
public class VoxelVerticalList : VerticalList<MapVoxel, VoxelVerticalListChunk> { }

public interface IVoxelStorage : IVoxelStorage<MapVoxel, VoxelVerticalList, VoxelVerticalListChunk> { }

[Serializable]
public class VoxelArrayOfVerticalListsVoxelStorage : ArrayOfVerticalListsVoxelStorage<MapVoxel, VoxelVerticalList, VoxelVerticalListChunk>, IVoxelStorage { }

[Serializable]
public class VoxelQuadTreeOfVerticalListsVoxelStorage : QuadTreeVoxelStorage<MapVoxel, VoxelVerticalList, VoxelVerticalListChunk>, IVoxelStorage { }
