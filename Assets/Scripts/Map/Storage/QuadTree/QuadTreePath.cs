﻿using System.Collections.Generic;

public struct QuadTreePath<T>
{
	private static Stack<Stack<QuadTreeNodeExt<T>>> stackPool = new Stack<Stack<QuadTreeNodeExt<T>>>();

	public Stack<QuadTreeNodeExt<T>> stack;

	public QuadTreePath(Stack<QuadTreeNodeExt<T>> stack)
	{
		this.stack = stack;
	}

	public static Stack<QuadTreeNodeExt<T>> Get()
	{
		if (stackPool.Count == 0)
		{
			return new Stack<QuadTreeNodeExt<T>>(QuadTree<T>.TREE_DEPTH);
		}
		return stackPool.Pop();
	}

	public static void Release(Stack<QuadTreeNodeExt<T>> stack)
	{
		stack.Clear();
		stackPool.Push(stack);
	}
}
