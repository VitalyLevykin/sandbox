﻿//using System;
//using System.Collections.Generic;
//using UnityEngine;
//
//public class VoxelRaycast
//{
//	public struct RayHit
//	{
//		public IntVector3 intPosition;
//		public Vector3 position;
//	}
//
//	private struct StackElement
//	{
//		public QuadTreeNode<VoxelVerticalList> node;
//	}
//
//	public struct QuadtreeWalker<T>
//	{
//		private QuadTree<T> quadTree;
//		private Vector3 rayStart;
//		private Vector3 direction;
//		private int nextX;
//		private int nextZ;
//		private QuadTreeNodeExt<T>? node;
//
//		private QuadTreePath<T> path; 
//
//		public QuadtreeWalker(QuadTree<T> quadTree, Vector3 rayStart, Vector3 direction)
//		{
//			this.quadTree = quadTree;
//			this.rayStart = rayStart;
//			this.direction = direction;
//
//			nextX = MapPositionConverter.FloorToInt(rayStart.x);
//			nextZ = MapPositionConverter.FloorToInt(rayStart.z);
//			this.node = null;//quadTree.GetClosestParentNode(nextX, nextZ);
//
//			path = new QuadTreePath<T>(QuadTreePath<T>.Get());
//		}
//
//		public void Dispose()
//		{
//			QuadTreePath<T>.Release(path.stack);
//		}
//
//		public bool MoveNext()
//		{
//			if (node == null)
//			{
//				// first MoveNext
//				quadTree.GetClosestParentNode(nextX, nextZ, path);
//				node = path.stack.Peek();
//			}
//			else
//			{
//				node = node.GetClosestParentNodeByPath(nextX, nextZ, path);
//			}
//			return node.Value.size != 1;
//		}
//
//		public T Current
//		{
//			get
//			{
////				if (node.size != 1)
////					throw new 
//				throw new NotImplementedException();
//			}
//		}
//
//		public void Reset()
//		{
//			throw new NotImplementedException();
//		}
//	}
//
//	public struct RayWalker // : IEnumerator<MapVoxel>
//	{
//		//
//	}
//
//	public static VoxelQuadTreeOfVerticalListsVoxelStorage storage;
//	public static QuadTree<VoxelVerticalList> quadTree;
//
//	public static RayHit? Raycast(Vector3 origin, Vector3 dir, float maxDistance)
//	{
//		// check args
//
//		IntVector3 intOrigin = MapPositionConverter.FloorToInt(origin);
//		QuadTreeNodeExt<VoxelVerticalList> originClosestParent = quadTree.GetClosestParentNode(intOrigin.x, intOrigin.z);
//
//		// choose direction
//		// up or down
//		// right-up
//		// right-down
//		// left-up
//		// left-down
//
//		var stack = new List<StackElement>(QuadTree<VoxelVerticalList>.TREE_DEPTH);
//
//		return null;
//	}
//}
