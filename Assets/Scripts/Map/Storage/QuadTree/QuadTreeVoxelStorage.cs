using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class QuadTreeVoxelStorage<T, TList, TChunk> : IVoxelStorage<T, TList, TChunk>, ISerializationCallbackReceiver
	where TList : VerticalList<T, TChunk>, new()
	where TChunk : VerticalListChunk<T>, new()
{
	public event VerticalList<T, TChunk>.OnAddDelegate OnAdd;
	public event VerticalList<T, TChunk>.OnAddNewDelegate OnAddNew;
//	public event VerticalList<T, TChunk>.OnInstantiateChunkDelegate OnInstantiateChunk;
	public event VerticalList<T, TChunk>.OnClearDelegate OnClear;
	public event VerticalList<T, TChunk>.OnMergeDelegate OnMerge;
	public event VerticalList<T, TChunk>.OnAfterChangeDelegate OnAfterChange;

	private QuadTree<TList> quadTree = new QuadTree<TList>();

	public QuadTree<TList> GetQuadTree()
	{
		return quadTree;
	}

	public T Get(IntVector3 position)
	{
		TList list = quadTree.Get(position.x, position.z);
		return list != null ? list.Get(position.y) : default(T);
	}

	public bool Has(IntVector3 position)
	{
		return Get(position) != null;
	}

	public void Set(IntVector3 position, T element)
	{
		TList list = quadTree.Get(position.x, position.z);

		// TODO optimize

		if (list == null)
		{
			list = new TList();
			list.SetXZ(position.x, position.z);
			quadTree.Set(position.x, position.z, list);
			AddListenerToVerticalList(list);
		}

		ExpandBounds(position);
		list.Set(position.y, element);
	}

	public void ClearElement(IntVector3 position)
	{
		var node = quadTree.GetNode(position.x, position.z);
		if (node != null)
		{
			var verticalList = node.element;
			if (verticalList != null)
			{
				verticalList.ClearElement(position.y);
				if (verticalList.IsEmpty())
				{
					node.element = null;
					RemoveListenerFromVerticalList(verticalList);
				}
			}
		}
	}

	public TList GetVerticalList(int x, int z)
	{
		return quadTree.Get(x, z);
	}

	public LinkedListNode<TChunk> GetVerticalListNode(IntVector3 position)
	{
		TList list = GetVerticalList(position.x, position.z);
		if (list != null)
		{
			return list.GetNode(position.y);
		}
		return null;
	}

	public string Dump()
	{
		throw new System.NotImplementedException();
	}

	public IntVector3 GetMinPosition()
	{
		int maxSize = QuadTree<TList>.MAX_SIZE;
		return new IntVector3(-maxSize, -maxSize, -maxSize);
	}

	public IntVector3 GetMaxPosition()
	{
		int maxSize = QuadTree<TList>.MAX_SIZE;
		return new IntVector3(maxSize - 1, maxSize - 1, maxSize - 1);
	}

	public IntVector3 BoundsMin
	{
		get { return boundsMin; }
	}

	public IntVector3 BoundsMax
	{
		get { return boundsMax; }
	}

	public IEnumerator<TList> GetEnumerator()
	{
		return quadTree.GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return GetEnumerator();
	}

	///////////// LISTENING
	//
	//	public event VerticalList<T, TChunk>.OnAddDelegate OnAdd;
	//	public event VerticalList<T, TChunk>.OnAddNewDelegate OnAddNew;
	//	//	public event VerticalList<T, TChunk>.OnRemoveDelegate OnRemove;
	//	public event VerticalList<T, TChunk>.OnInstantiateChunkDelegate OnInstantiateChunk;
	//	public event VerticalList<T, TChunk>.OnClearDelegate OnClear;
	//	public event VerticalList<T, TChunk>.OnMergeDelegate OnMerge;

	//	[SerializeField]
	//private TList[] ar = new TList[MAX_SIZE * MAX_SIZE];

	// TODO
	[SerializeField]
	private List<TList> lists = new List<TList>();

	private IntVector3 boundsMin;
	private IntVector3 boundsMax;

	public void OnBeforeSerialize()
	{
//		AddListenerToAllLists();
		lists.Clear();
		foreach (TList list in quadTree)
		{
			lists.Add(list);
			AddListenerToVerticalList(list);
		}
	}

	public void OnAfterDeserialize()
	{
		boundsMin = new IntVector3(int.MaxValue, int.MaxValue, int.MaxValue);
		boundsMax = new IntVector3(int.MinValue, int.MinValue, int.MinValue);
//		AddListenerToAllLists();
		quadTree.ClearAll();
		foreach (TList list in lists)
		{
			ExpandBounds(new IntVector3(list.x, list.GetBottom(), list.z));
			ExpandBounds(new IntVector3(list.x, list.GetTop(), list.z));

			quadTree.Set(list.x, list.z, list);
			AddListenerToVerticalList(list);
		}
	}

	private void ExpandBounds(IntVector3 position)
	{
		if (boundsMin.x > position.x)
			boundsMin.x = position.x;
		if (boundsMin.y > position.y)
			boundsMin.y = position.y;
		if (boundsMin.z > position.z)
			boundsMin.z = position.z;

		if (boundsMax.x < position.x)
			boundsMax.x = position.x;
		if (boundsMax.y < position.y)
			boundsMax.y = position.y;
		if (boundsMax.z < position.z)
			boundsMax.z = position.z;
	}

	// from VL
	//	public void OnBeforeSerialize()
	//	{
	//		chunksSer.Clear();
	//		foreach (var chunk in chunks)
	//		{
	//			var chunkSer = new TChunk();
	//			chunkSer.Init(chunk.Element, chunk.Top, chunk.Length, false);
	//			chunksSer.Add(chunkSer);
	//		}
	//	}
	//
	//	public void OnAfterDeserialize()
	//	{
	//		chunks.Clear();
	//		foreach (var chunk in chunksSer)
	//		{
	//			chunks.AddLast((TChunk)new TChunk().Init(chunk.Element, chunk.Top, chunk.Length, false));
	//		}
	//	}


	private void AddListenerToAllLists()
	{
		foreach (TList list in quadTree)
		{
			AddListenerToVerticalList(list);
		}
	}

	private void AddListenerToVerticalList(TList verticalList)
	{
		if (verticalList.onAdd == null) // TODO without check, multiple add possible
			verticalList.onAdd += OnAddInVerticalList;
		if (verticalList.onAddNew == null)
			verticalList.onAddNew += OnAddNewInVerticalList;
		//		verticalList.onRemove += OnRemoveInVerticalList;
//		if (verticalList.onInstantiateChunk == null)
//			verticalList.onInstantiateChunk += OnInstantiateChunkInVerticalList;
		if (verticalList.onClear == null)
			verticalList.onClear += OnClearInVerticalList;
		if (verticalList.onMerge == null)
			verticalList.onMerge += OnMergeInVerticalList;
		if (verticalList.onAfterChange == null)
			verticalList.onAfterChange += OnAfterChangeInVerticalList;
	}

	private void RemoveListenerFromVerticalList(TList verticalList)
	{
		verticalList.onAdd -= OnAddInVerticalList;
		verticalList.onAddNew -= OnAddNewInVerticalList;
		//		verticalList.onRemove -= OnRemoveInVerticalList;
//		verticalList.onInstantiateChunk -= OnInstantiateChunkInVerticalList;
		verticalList.onClear -= OnClearInVerticalList;
		verticalList.onMerge -= OnMergeInVerticalList;
		verticalList.onAfterChange -= OnAfterChangeInVerticalList;
	}

	private void OnAddInVerticalList(IntVector3 position, int prevTop, int prevBottom, TChunk chunk)
	{
		OnAdd(position, prevTop, prevBottom, chunk);
	}

	private void OnAddNewInVerticalList(IntVector3 position, TChunk chunk)
	{
		OnAddNew(position, chunk);
	}

	//	private void OnRemoveInVerticalList(IntVector3 position, int prevTop, int prevBottom, TChunk chunk)
	//	{
	//		OnRemove(position, prevTop, prevBottom, chunk);
	//	}

//	private void OnInstantiateChunkInVerticalList(TChunk chunk, int x, int z)
//	{
//		OnInstantiateChunk(chunk, x, z);
//	}

	private void OnClearInVerticalList(IntVector3 position, int prevTop, int prevBottom, TChunk chunk, TChunk newLowerChunk)
	{
		OnClear(position, prevTop, prevBottom, chunk, newLowerChunk);
	}

	private void OnMergeInVerticalList(IntVector3 position, TChunk chunk, TChunk nextChunk)
	{
		OnMerge(position, chunk, nextChunk);
	}

	private void OnAfterChangeInVerticalList(IntVector3 position, TChunk chunk)
	{
		OnAfterChange(position, chunk);
	}
}
