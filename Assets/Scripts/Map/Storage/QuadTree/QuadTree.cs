﻿using System;
using System.Collections;
using System.Collections.Generic;

public class QuadTree<T> : IEnumerable<T>
{
	/*private*/public QuadTreeNode<T> root = new QuadTreeNode<T>();

	public static readonly int MAX_SIZE = 524288; // depth=20
	public static readonly int TREE_DEPTH = 20;

	public QuadTree()
	{
	}

	public T Get(int x, int z)
	{
		var node = GetNode(x, z);
		return node != null ? node.element : default(T);
	}

	public QuadTreeNode<T> GetNode(int x, int z)
	{
		var node = GetClosestParentNode(x, z);
		if (node.size != 1)
			return null;
		return node.node;
	}

	public QuadTreeNodeExt<T> GetClosestParentNode(int x, int z)
	{
		var path = new QuadTreePath<T>(QuadTreePath<T>.Get());
		try
		{
			GetClosestParentNode(x, z, path);
			return path.stack.Peek();
		}
		finally
		{
			QuadTreePath<T>.Release(path.stack);
		}
	}

	public void GetClosestParentNode(int x, int z, QuadTreePath<T> path)
	{
		CheckRange(x, z);

		GetClosestParentNode1(x, z, path, root);
//		var result = new QuadTreeNodeExt<T>();
//		result.node = node;
//		result.parent = prevNode;
//		result.x = offsetX;
//		result.z = offsetZ;
//		result.size = size;
//		return result;
	}

	public void GetClosestParentNode1(int x, int z, QuadTreePath<T> path, QuadTreeNode<T> nextNode)
	{
		QuadTreeNode<T> node = null;
		QuadTreeNode<T> prevNode = null;
		int offsetX = -MAX_SIZE;
		int offsetZ = -MAX_SIZE;
		int size = MAX_SIZE + MAX_SIZE;
		int nextOffsetX = -MAX_SIZE;
		int nextOffsetZ = -MAX_SIZE;
		int nextSize = size;

		while (nextNode != null)
		{
			prevNode = node;
			node = nextNode;
			offsetX = nextOffsetX;
			offsetZ = nextOffsetZ;
			size = nextSize;

			nextSize = nextSize / 2;
			int centerX = nextOffsetX + nextSize;
			int centerZ = nextOffsetZ + nextSize;

			if (x >= centerX)
			{
				if (z >= centerZ)
				{
					nextNode = node.m11;
					nextOffsetX = centerX;
					nextOffsetZ = centerZ;
				}
				else
				{
					nextNode = node.m10;
					nextOffsetX = centerX;
				}
			}
			else
			{
				if (z >= centerZ)
				{
					nextNode = node.m01;
					nextOffsetZ = centerZ;
				}
				else
				{
					nextNode = node.m00;
				}
			}

			path.stack.Push(new QuadTreeNodeExt<T> {node = node, parent = prevNode, size = size, x = offsetX, z = offsetZ});
		}
	}

//	public T GetOrCreate(int x, int z)
//	{
//		return null;
//	}

	public T Set(int x, int z, T element)
	{
		if (element == null)
			throw new ArgumentNullException("element");
		CheckRange(x, z);

		int size = MAX_SIZE + MAX_SIZE;

		QuadTreeNode<T> nextNode = root;
		int nextSize = size / 2;
		int nextOffsetX = -MAX_SIZE;
		int nextOffsetZ = -MAX_SIZE;

		while (nextSize != 0)
		{
			QuadTreeNode<T> node = nextNode;
			int centerX = nextOffsetX + nextSize;
			int centerZ = nextOffsetZ + nextSize;
			nextSize = nextSize / 2;

			if (x >= centerX)
			{
				if (z >= centerZ)
				{
					if (node.m11 == null)
						node.m11 = new QuadTreeNode<T>();
					nextNode = node.m11;
					nextOffsetX = centerX;
					nextOffsetZ = centerZ;
				}
				else
				{
					if (node.m10 == null)
						node.m10 = new QuadTreeNode<T>();
					nextNode = node.m10;
					nextOffsetX = centerX;
				}
			}
			else
			{
				if (z >= centerZ)
				{
					if (node.m01 == null)
						node.m01 = new QuadTreeNode<T>();
					nextNode = node.m01;
					nextOffsetZ = centerZ;
				}
				else
				{
					if (node.m00 == null)
						node.m00 = new QuadTreeNode<T>();
					nextNode = node.m00;
				}
			}
		}

		T prevElement = nextNode.element;
		nextNode.element = element;
		return prevElement;
	}

	public T Clear(int x, int z)
	{
		QuadTreeNode<T> node = GetNode(x, z);
		if (node == null)
			return default(T);

		T prevElement = node.element;
		node.element = default(T);
		return prevElement;

		// TODO remove empty nodes without children
	}

	public void ClearAll()
	{
		root = new QuadTreeNode<T>();
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return GetEnumerator();
	}

	// must enumerate recursively - see SceneBatcher
	public IEnumerator<T> GetEnumerator()
	{
		return EnumerateAndYield(root).GetEnumerator();
	}

	private IEnumerable<T> EnumerateAndYield(QuadTreeNode<T> node)
	{
		if (node.element != null)
		{
			yield return node.element;
		}

		if (node.m00 != null)
		{
			foreach (T e in EnumerateAndYield(node.m00))
			{
				yield return e;
			}
		}
		if (node.m10 != null)
		{
			foreach (T e in EnumerateAndYield(node.m10))
			{
				yield return e;
			}
		}
		if (node.m01 != null)
		{
			foreach (T e in EnumerateAndYield(node.m01))
			{
				yield return e;
			}
		}
		if (node.m11 != null)
		{
			foreach (T e in EnumerateAndYield(node.m11))
			{
				yield return e;
			}
		}
	}

	private static void CheckRange(int x, int z)
	{
		if (x < -MAX_SIZE || x >= MAX_SIZE || z < -MAX_SIZE || z >= MAX_SIZE)
			throw new Exception("Out of range: x=" + x + " z=" + z);
	}
}
