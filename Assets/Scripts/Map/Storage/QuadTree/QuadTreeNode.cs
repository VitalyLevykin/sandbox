﻿public class QuadTreeNode<T>
{
	public T element;
	public QuadTreeNode<T> m00;
	public QuadTreeNode<T> m01;
	public QuadTreeNode<T> m10;
	public QuadTreeNode<T> m11;
}
