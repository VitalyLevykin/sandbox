using System.Collections.Generic;

public interface IVoxelStorage<T, TList, TChunk> : IEnumerable<TList>
	where TList : VerticalList<T, TChunk>, new()
	where TChunk : VerticalListChunk<T>, new()
{
	event VerticalList<T, TChunk>.OnAddDelegate OnAdd;
	event VerticalList<T, TChunk>.OnAddNewDelegate OnAddNew;
	//	event VerticalList<T, TChunk>.OnRemoveDelegate OnRemove;
//	event VerticalList<T, TChunk>.OnInstantiateChunkDelegate OnInstantiateChunk;
	event VerticalList<T, TChunk>.OnClearDelegate OnClear;
	event VerticalList<T, TChunk>.OnMergeDelegate OnMerge;
	event VerticalList<T, TChunk>.OnAfterChangeDelegate OnAfterChange;

	
	T Get(IntVector3 position);
	
	bool Has(IntVector3 position);
	
	void Set(IntVector3 position, T value);

	void ClearElement(IntVector3 position);

	TList GetVerticalList(int x, int z);

	LinkedListNode<TChunk> GetVerticalListNode(IntVector3 position);

	string Dump();

	IntVector3 GetMinPosition();

	IntVector3 GetMaxPosition();

	IntVector3 BoundsMin { get; }
	IntVector3 BoundsMax { get; }
}
