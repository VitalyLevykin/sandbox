﻿using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
public class VoxelStorageComponent : BaseBehaviour
{
	[SerializeField]
	[HideInInspector]
	private VoxelQuadTreeOfVerticalListsVoxelStorage storage = new VoxelQuadTreeOfVerticalListsVoxelStorage();
	[HideInInspector]
	public GameObject perlinNoiseObject;

	void Awake()
	{
		// hack to avoid serialization of this (BIG) component while drawing inspector
		hideFlags = HideFlags.HideInInspector;
	}

	public IVoxelStorage GetStorage()
	{
		return storage;
	}

	public void Stats()
	{
		int voxels = storage.SelectMany(verticalList => verticalList.GetChunks()).Sum(chunk => chunk.Length);

		Debug.Log("Voxels=" + voxels);
	}

	public void Dump()
	{
		Debug.Log(GetStorage().Dump());
//		((VoxelQuadTreeOfVerticalListsVoxelStorage) GetStorage()).CollectBatches();
	}

	public void PerlinNoise()
	{
		perlinNoiseObject.GetInstanceID();
		float r = Random.value;

		ClearPerlinNoise();

		var ambientShadingBuilder = GetComponent<AmbientShadingBuilder>();
		bool ambientShadingBuilderEnabled = ambientShadingBuilder != null && ambientShadingBuilder.enabled;
		if (ambientShadingBuilderEnabled)
			ambientShadingBuilder.enabled = false;

		for (int x = 0; x < 30; x++)
		{
			for (int z = 0; z < 30; z++)
			{
				float value = Mathf.PerlinNoise(r + x*0.1f, z*0.1f);
				value -= 0.2f;
				value *= 1.5f;
				value = Mathf.Clamp01(value);
				for (int y = 0; y * 0.3f < value; y++)
				{
					IntVector3 position = new IntVector3(x, y, 20 + z);
					var voxel = new MapVoxel();
					voxel.Init(perlinNoiseObject, 0, false);
					storage.Set(position, voxel);
				}
			}
		}

		if (ambientShadingBuilderEnabled)
			ambientShadingBuilder.enabled = true;
	}

	public void ClearPerlinNoise()
	{
		for (int x = 0; x < 30; x++)
		{
			for (int z = 0; z < 30; z++)
			{
				float value = 1;
				for (int y = 0; y * 0.2f < value; y++)
				{
					IntVector3 position = new IntVector3(x, y, 20 + z);
					storage.ClearElement(position);
				}
			}
		}
	}
}
