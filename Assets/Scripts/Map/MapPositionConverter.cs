using UnityEngine;

public class MapPositionConverter
{
	public static IntVector3 FloorToInt(Vector3 position)
	{
		return new IntVector3(FloorToInt(position.x), Mathf.FloorToInt(position.y), Mathf.FloorToInt(position.z));
	}

	public static int FloorToInt(float value)
	{
		int floorToInt = Mathf.FloorToInt(value);
		if (value - floorToInt > 0.999f)
			return floorToInt + 1;
		return floorToInt;
	}

	public static IntVector3 CeilToInt(Vector3 position)
	{
		return new IntVector3(Mathf.CeilToInt(position.x), Mathf.CeilToInt(position.y), Mathf.CeilToInt(position.z));
	}

	public static Vector3 ToFloat(IntVector3 position)
	{
		return new Vector3(position.x, position.y, position.z);
	}

	public static Vector3 ToViewPivot(IntVector3 position)
	{
//		return new Vector3(position.x + 0.5f, position.y + 0.5f, position.z + 0.5f);
		return new Vector3(position.x + 0.5f, position.y, position.z + 0.5f);
	}
}
