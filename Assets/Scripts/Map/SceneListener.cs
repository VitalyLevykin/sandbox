﻿using System;
using System.Collections.Generic;

public class SceneListener
{
	public static Action OnSceneLoad;
	public static Action OnSceneSave;

	private static HashSet<Action> loadListeners = new HashSet<Action>();
	private static HashSet<Action> saveListeners = new HashSet<Action>();

	public static void AddLoadListenerIfNotAdded(Action listener)
	{
		if (loadListeners.Add(listener))
		{
			UnityEngine.Debug.Log("AddLoadListener");
			OnSceneLoad += listener;
		}
	}

	public static void AddSaveListenerIfNotAdded(Action listener)
	{
		if (saveListeners.Add(listener))
		{
			UnityEngine.Debug.Log("AddSaveListener");
			OnSceneSave += listener;
		}
	}

	public static void SendLoadEvent()
	{
		UnityEngine.Debug.Log("SendLoadEvent");
		if (OnSceneLoad != null)
			OnSceneLoad();
	}

	public static void SendSaveEvent()
	{
		UnityEngine.Debug.Log("SendSaveEvent");
		if (OnSceneSave != null)
			OnSceneSave();
	}
}
