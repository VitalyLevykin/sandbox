﻿using System;
using UnityEngine;

[Serializable]
// TODO struct
public class MapVoxelData : IEquatable<MapVoxelData> //: ICloneable
{
	[SerializeField]
	public GameObject storedObject;
	[SerializeField]
	public int rotationY;
	[SerializeField]
	public bool split;
	
//	public object Clone()
//	{
//		return null;
//	}

	public bool Equals(MapVoxelData other)
	{
		return Equals(storedObject, other.storedObject) && Equals(rotationY, other.rotationY) && Equals(split, other.split);
	}

	public override string ToString()
	{
		return string.Format("[MapVoxelData StoredObject={0} RotationY={1} Split={2}]", storedObject, rotationY, split);
	}
}
