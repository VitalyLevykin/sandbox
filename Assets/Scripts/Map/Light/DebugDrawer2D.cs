﻿using System.Collections.Generic;
using UnityEngine;

public class DebugDrawer2D : BaseBehaviour
{
	public static Material lineMaterial;
	private List<Line> lines = new List<Line>();

	private struct Line
	{
		public Vector2 from;
		public Vector2 to;
		public Color color;
	}

	public void DrawLine(Vector2 from, Vector2 to, Color color)
	{
		lines.Add(new Line { from = from, to = to, color = color });
	}

	private static void CreateLineMaterial()
	{
		if (!lineMaterial)
		{
			lineMaterial =
				new Material("Shader \"Lines/Colored Blended\" {" + "SubShader { Pass { " + "    Blend SrcAlpha OneMinusSrcAlpha " + "    ZWrite Off Cull Off Fog { Mode Off } " +
							"    BindChannels {" + "      Bind \"vertex\", vertex Bind \"color\", color }" + "} } }");
			lineMaterial.hideFlags = HideFlags.HideAndDontSave;
			lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
		}
	}

	private void OnPostRender()
	{
		CreateLineMaterial();
		lineMaterial.SetPass(0);
		GL.LoadOrtho();
		GL.Begin(GL.LINES);
		foreach (Line line in lines)
		{
			GL.Color(line.color);
			GL.Vertex(new Vector3(line.from.x / Screen.width, line.from.y / Screen.height, 0));
			GL.Vertex(new Vector3(line.to.x / Screen.width, line.to.y / Screen.height, 0));
		}
		GL.End();

		lines.Clear();
	}
}
