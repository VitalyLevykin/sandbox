﻿using UnityEngine;

public class LocalRotation : MonoBehaviour
{
	public Vector3 rotationSpeed;
	
	void Update ()
	{
		Quaternion rotation = Quaternion.Euler(rotationSpeed * Time.deltaTime);
		transform.localRotation *= rotation;
	}
}
