﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MapVoxel : ICloneable
{
	[SerializeField]
	private MapVoxelData data = new MapVoxelData();

	[NonSerialized]
	public List<GameObject> ViewObjects = new List<GameObject>(); // lower objects first

	public MapVoxelData Data
	{
		get { return data; }
	}

	public GameObject StoredObject
	{
		get { return data.storedObject; }
	}

	public int RotationY
	{
		get { return data.rotationY; }
	}

	public bool Split
	{
		get { return data.split; }
	}

	public MapVoxel Init(GameObject storedObject, int rotationY, bool split)
	{
		if (storedObject == null)
			throw new ArgumentNullException("storedObject");
		data.storedObject = storedObject;
		data.rotationY = rotationY;
		data.split = split;
		return this;
	}

	public object Clone()
	{
		return new MapVoxel().Init(data.storedObject, data.rotationY, data.split);
	}

	public GameObject GetAndRemoveViewObject(int index)
	{
		GameObject viewObject;
		try
		{
			viewObject = ViewObjects[index];
		}
		catch (ArgumentOutOfRangeException e)
		{
			Debug.Log("index=" + index);
			throw;
		}
		ViewObjects.RemoveAt(index);
		return viewObject;
	}

	protected bool Equals(MapVoxel other)
	{
		// once created, MapVoxel should never change // TODO fix VerticalList.onReplace
		return Equals(data.storedObject, other.data.storedObject) && data.rotationY == other.data.rotationY && data.split == false && other.data.split == false;
	}

	public override bool Equals(object obj)
	{
		if (ReferenceEquals(null, obj)) return false;
		if (ReferenceEquals(this, obj)) return true;
		if (obj.GetType() != this.GetType()) return false;
		return Equals((MapVoxel) obj);
	}

//	public override int GetHashCode()
//	{
//		unchecked
//		{
//			int hashCode = base.GetHashCode();
//			hashCode = (hashCode*397) ^ (storedObject != null ? storedObject.GetHashCode() : 0);
//			hashCode = (hashCode*397) ^ rotationY;
//			hashCode = (hashCode*397) ^ split.GetHashCode();
//			return hashCode;
//		}
//	}

	public override string ToString()
	{
		return string.Format("[MapVoxel Data={0} ViewObjects={1}]", data, ViewObjects);
	}
}
