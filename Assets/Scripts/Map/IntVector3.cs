using System;
using UnityEngine;

public struct IntVector3
{
	public int x;
	public int y;
	public int z;
	
	public IntVector3(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public static IntVector3 operator +(IntVector3 a, IntVector3 b)
	{
		return new IntVector3(a.x + b.x, a.y + b.y, a.z + b.z);
	}

	public static IntVector3 operator -(IntVector3 a, IntVector3 b)
	{
		return new IntVector3(a.x - b.x, a.y - b.y, a.z - b.z);
	}

	public static IntVector3 operator -(IntVector3 a)
	{
		return new IntVector3(-a.x, -a.y, -a.z);
	}

//	public static IntVector3 operator *(IntVector3 a, float d)
//	{
//		return new IntVector3(a.x * d, a.y * d, a.z * d);
//	}
//
//	public static IntVector3 operator *(float d, IntVector3 a)
//	{
//		return new IntVector3(a.x * d, a.y * d, a.z * d);
//	}
//
//	public static IntVector3 operator /(IntVector3 a, float d)
//	{
//		return new IntVector3(a.x / d, a.y / d, a.z / d);
//	}

	public static bool operator ==(IntVector3 v1, IntVector3 v2)
	{
		return v1.x == v2.x && v1.y == v2.y && v1.z == v2.z;
	}
	
	public static bool operator!=(IntVector3 v1, IntVector3 v2)
	{
		return v1.x != v2.x || v1.y != v2.y || v1.z != v2.z;
	}

	public static IntVector3 zero
	{
		get
		{
			return new IntVector3(0, 0, 0);
		}
	}

	public static IntVector3 one
	{
		get
		{
			return new IntVector3(1, 1, 1);
		}
	}

	public static IntVector3 forward
	{
		get
		{
			return new IntVector3(0, 0, 1);
		}
	}

	public static IntVector3 back
	{
		get
		{
			return new IntVector3(0, 0, -1);
		}
	}

	public static IntVector3 up
	{
		get
		{
			return new IntVector3(0, 1, 0);
		}
	}

	public static IntVector3 down
	{
		get
		{
			return new IntVector3(0, -1, 0);
		}
	}

	public static IntVector3 left
	{
		get
		{
			return new IntVector3(-1, 0, 0);
		}
	}

	public static IntVector3 right
	{
		get
		{
			return new IntVector3(1, 0, 0);
		}
	}

	public override bool Equals(object obj)
	{
		if (!(obj is IntVector3))
			return false;
		return this == (IntVector3)obj;
	}

	public override int GetHashCode()
	{
		return x.GetHashCode() ^ y.GetHashCode() << 2 ^ z.GetHashCode() >> 2;
	}

	public Vector3 ToVector3()
	{
		return new Vector3(x, y, z);
	}

	public static IntVector3 Min(IntVector3 lhs, IntVector3 rhs)
	{
		return new IntVector3(Math.Min(lhs.x, rhs.x), Math.Min(lhs.y, rhs.y), Math.Min(lhs.z, rhs.z));
	}

	public static IntVector3 Max(IntVector3 lhs, IntVector3 rhs)
	{
		return new IntVector3(Math.Max(lhs.x, rhs.x), Math.Max(lhs.y, rhs.y), Math.Max(lhs.z, rhs.z));
	}

	public override string ToString()
	{
		return string.Format ("[IntVector3: x={0}, y={1}, z={2}]", x, y, z);
	}
}
