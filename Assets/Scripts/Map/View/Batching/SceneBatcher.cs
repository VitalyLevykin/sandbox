﻿using UnityEngine;

public class SceneBatcher : BaseBehaviour
{
	public void CollectBatches()
	{
		var voxelStorageComponent = GetComponent<VoxelStorageComponent>();
		IVoxelStorage voxelStorage = voxelStorageComponent.GetStorage();

		const int BATCH_SIZE = 16; // must be power-of-two

		IntVector3 prevBatchPosition = new IntVector3(int.MinValue, int.MinValue, int.MinValue);
		Batcher batcher = null;

		foreach (var list in voxelStorage)
		{
			IntVector3 position = new IntVector3(list.x, 0, list.z);
			IntVector3 batchPosition = new IntVector3(Mathf.FloorToInt(position.x / (float)BATCH_SIZE), 0, Mathf.FloorToInt(position.z / (float)BATCH_SIZE));

			if (prevBatchPosition != batchPosition)
			{
				if (prevBatchPosition.x != int.MinValue)
				{
					// end
					Debug.Log("End batch " + prevBatchPosition);
					EndBatch(batcher);
				}
				prevBatchPosition = batchPosition;

				// begin
				Debug.Log("Begin batch " + batchPosition);
				batcher = BeginBatch(batchPosition);
			}

//			Debug.Log("Add to batch " + batchPosition + " " + position);
			AddToBatch(batcher, list);
		}
	}

	private Batcher BeginBatch(IntVector3 batchPosition)
	{
		Matrix4x4 rootTransform = Matrix4x4.TRS(batchPosition.ToVector3(), Quaternion.identity, Vector3.one);
		return new Batcher(rootTransform);
	}

	private void AddToBatch(Batcher batcher, VoxelVerticalList list)
	{
		if (list.IsEmpty())
			return;

		foreach (VoxelVerticalListChunk chunk in list.GetChunks())
		{
			MapVoxel mapVoxel = chunk.Element;
			foreach (GameObject viewObject in mapVoxel.ViewObjects)
			{
				batcher.Add(viewObject);
//				DestroyImmediate(viewObject);
			}
			mapVoxel.ViewObjects.Clear();
		}
	}

	private void EndBatch(Batcher batcher)
	{
		Debug.Log("SceneBatcher.EndBatch");
		var sceneBuilder = GetComponent<SceneBuilder>();
		//GameObject viewObjectsContainer = sceneBuilder.ClearViewObjects();
		GameObject viewObjectsContainer = transform.GetOrCreateChild("Batches");

		foreach (var material in batcher.Materials)
		{
			Mesh mesh = batcher.Combine(material);

			var batch = new GameObject("Batch");
			batch.transform.parent = viewObjectsContainer.transform;

			batch.AddComponent<MeshFilter>().sharedMesh = mesh;
			var meshRenderer = batch.AddComponent<MeshRenderer>();
			meshRenderer.sharedMaterial = material;
		}
	}
}
