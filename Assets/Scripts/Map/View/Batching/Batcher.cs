﻿using System.Collections.Generic;
using UnityEngine;

public class Batcher
{
	private Dictionary<Material, List<CombineInstance>> materialToMeshes = new Dictionary<Material, List<CombineInstance>>();
	private Matrix4x4 rootTransform;
	private Matrix4x4 invRootTransform;

	public Batcher(Matrix4x4 rootTransform)
	{
		this.rootTransform = rootTransform;
		this.invRootTransform = rootTransform.inverse;
	}

	public void Add(GameObject viewObject)
	{
		foreach (Renderer renderer in viewObject.GetComponentsInChildren<Renderer>())
		{
			Material[] sharedMaterials = renderer.sharedMaterials;
			var meshFilter = renderer.GetComponent<MeshFilter>();

			if (sharedMaterials.Length == 0)
			{
				if (meshFilter != null)
					Debug.LogWarning("Batcher: renderer without materials, but with MeshFilter: " + renderer);
				continue;
			}
			if (sharedMaterials.Length > 1)
				Debug.LogWarning("Batcher: many materials in renderer " + renderer);

			Material material = sharedMaterials[0];
			if (material == null)
			{
				Debug.LogWarning("Batcher: null material in renderer " + renderer);
				continue;
			}

			if (meshFilter == null)
			{
				Debug.LogWarning("Batcher: renderer without MeshFilter, but with material: " + renderer);
				continue;
			}
			var mesh = meshFilter.sharedMesh;
			Matrix4x4 transform = invRootTransform * renderer.transform.localToWorldMatrix;
			var combineInstance = new CombineInstance {mesh = mesh, transform = transform};

			List<CombineInstance> list;
			if (!materialToMeshes.TryGetValue(material, out list))
			{
				list = new List<CombineInstance>();
				materialToMeshes.Add(material, list);
			}
			list.Add(combineInstance);
		}
	}

	public IEnumerable<Material> Materials
	{
		get { return materialToMeshes.Keys; }
	}

	public Mesh Combine(Material material)
	{
		var meshes = materialToMeshes[material];
		var mesh = new Mesh();
		mesh.CombineMeshes(meshes.ToArray(), true, true);
		return mesh;
	}
}
