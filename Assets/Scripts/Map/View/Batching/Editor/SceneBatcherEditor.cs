﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SceneBatcher))]
public class SceneBatcherEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		var sceneBatcher = (SceneBatcher)target;

		if (GUILayout.Button("Batch"))
		{
			sceneBatcher.CollectBatches();
		}
	}
}
