﻿using System.Linq;
using UnityEngine;

[RequireComponent(typeof(VoxelStorageComponent))]
public class AmbientShadingBuilder : BaseBehaviour, SceneBuilderEvent
{
	private const float SHADING_SNAP_EPSILON = 0.05f;

	public float ambientShadingStrength = 0.5f;
	public bool shadowColorIsLightColor;
	public bool ambientShadowColorIsLightColor;
	public Color shadowColor = Color.black;
	public Color ambientShadowColor = Color.black;

	public bool replaceShader = true;
	public Shader shaderReplaceFrom;
	public Shader ambientShader;

	public bool highlightReinstantiated;

	private IVoxelStorage storage;
	private static VoxelAmbientShadingData cachedVoxelShading = new VoxelAmbientShadingData();

	IVoxelStorage Storage()
	{
		return storage ?? (storage = GetComponent<VoxelStorageComponent>().GetStorage());
	}

	public void FillShadingToMesh(IntVector3 voxelPosition, Mesh mesh, Matrix4x4 transform)
	{
		Vector3[] vertices = mesh.vertices;
		Color[] colors = mesh.colors;
		Vector3[] normals = mesh.normals;
		if (colors.Length == 0)
		{
			colors = new Color[mesh.vertexCount];
			for (int i = 0; i < mesh.vertexCount; i++)
				colors[i] = Color.white;
		}
		for (int i = 0; i < mesh.vertexCount; i++)
		{
			Vector3 position = transform.MultiplyPoint(vertices[i]);
			Vector3 normal = transform.MultiplyVector(normals[i]);
			float shading = GetShading(voxelPosition, position, normal);
//			colors[i] = Color.Lerp(Color.white, Color.gray, shading);
			colors[i] = Color.white;
//			colors[i].a = 0;
			colors[i].a = 1 - shading;//(1 - shading) * 0.5f + 0.5f;
			if (highlightReinstantiated)
				colors[i].a = 0;
		}
		mesh.colors = colors;
	}

	public void OnViewObjectInstantiate(GameObject viewObject, IntVector3 position)
	{
		if (viewObject.GetComponent<MeshFilter>() == null)
		{
			Debug.LogWarningFormat("No MeshFilter in instantiated gameObject {0}", viewObject);
			return;
		}

		Mesh sharedMesh = viewObject.GetComponent<MeshFilter>().sharedMesh;
		if (sharedMesh == null)
		{
			Debug.LogWarningFormat("Empty mesh in MeshFilter in instantiated gameObject {0}", viewObject);
			return;
		}

		Mesh mesh = Instantiate(sharedMesh);
//		mesh.bounds = new Bounds(new Vector3(0, 0.5f, 0), new Vector3(1, 1, 1));
		FillShadingToMesh(position, mesh, viewObject.transform.localToWorldMatrix);
		viewObject.GetComponent<MeshFilter>().sharedMesh = mesh;
		
		// TODO destroy mesh on disable

		var r = viewObject.GetComponent<Renderer>();
		if (r != null)
		{
			foreach (var material in r.sharedMaterials)
			{
				if (replaceShader && material.shader == shaderReplaceFrom)
				{
					material.shader = ambientShader;
					material.SetFloat("_AmbientShadingStrength", ambientShadingStrength);
					material.SetColor("_ShadowColor", shadowColor);
					material.SetColor("_AmbientShadowColor", ambientShadowColor);
				}

				if (!replaceShader && material.shader == ambientShader)
				{
					material.shader = shaderReplaceFrom;
				}
			}
		}

		// TODO destroy materials on disable
	}

	public void OnViewObjectInstantiated(IntVector3 position)
	{
		var sceneBuilder = GetComponent<SceneBuilder>();
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.left + IntVector3.down + IntVector3.back);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.left + IntVector3.down);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.left + IntVector3.down + IntVector3.forward);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.left + IntVector3.back);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.left);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.left + IntVector3.forward);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.left + IntVector3.up + IntVector3.back);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.left + IntVector3.up);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.left + IntVector3.up + IntVector3.forward);

		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.down + IntVector3.back);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.down);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.down + IntVector3.forward);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.back);

		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.forward);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.up + IntVector3.back);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.up);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.up + IntVector3.forward);

		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.right + IntVector3.down + IntVector3.back);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.right + IntVector3.down);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.right + IntVector3.down + IntVector3.forward);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.right + IntVector3.back);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.right);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.right + IntVector3.forward);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.right + IntVector3.up + IntVector3.back);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.right + IntVector3.up);
		sceneBuilder.ReinstantiateViewObjectAt(position + IntVector3.right + IntVector3.up + IntVector3.forward);
	}

	float GetShading(IntVector3 floorPosition, Vector3 position, Vector3 normal)
	{
//		IntVector3 floorPosition = MapPositionConverter.FloorToInt(position);
		Vector3 floorPositionFloat = MapPositionConverter.ToFloat(floorPosition);
		Vector3 localPosition = position - floorPositionFloat;

		float shading = 0;
		for (int y = 1; y >= 0; y--)
		{
			for (int x = 0; x <= 1; x++)
			{
				for (int z = 0; z <= 1; z++)
				{
					IntVector3 localCornerPosition = new IntVector3(x, y, z);
					float weight = GetWeight(localCornerPosition, ref localPosition);
					if (weight == 0)
						continue;

//					IntVector3 cornerPosition = floorPosition + localCornerPosition;
					float cornerShading = GetCornerShading(floorPosition, localCornerPosition, normal);

					if (weight == 1)
						return cornerShading;

					shading += weight * cornerShading;
				}
			}
		}
		return Mathf.Clamp01(shading);
	}

	float GetCornerShading(IntVector3 floorPosition, IntVector3 localCornerPosition, Vector3 normal)
	{
		IntVector3 normalSnapped = SnapNormal(normal);
		IntVector3 voxelPosition = GetVoxelPosition(floorPosition, localCornerPosition, normalSnapped);
		IntVector3 localCornerPositionToVoxel = (floorPosition + localCornerPosition) - voxelPosition;
		if (Storage().Has(voxelPosition))
		{
//			if (localCornerPositionToVoxel.x < 0 || localCornerPositionToVoxel.x > 1 || localCornerPositionToVoxel.y < 0 || localCornerPositionToVoxel.y > 1 || localCornerPositionToVoxel.z < 0 || localCornerPositionToVoxel.z > 1)
//				throw new Exception("localCornerPosition=" + localCornerPosition + " voxelPosition=" + voxelPosition + " localCornerPositionToVoxel=" + localCornerPositionToVoxel + " normal=" + normal);
			return GetCornerShadingViaCache(voxelPosition, localCornerPositionToVoxel, normalSnapped);
		}
		return 0;
	}

	float GetCornerShadingViaCache(IntVector3 voxelPosition, IntVector3 localCornerPosition, IntVector3 normalSnapped)
	{
		if (voxelPosition != cachedVoxelShading.voxelPosition)
		{
			cachedVoxelShading.voxelPosition = voxelPosition;
			cachedVoxelShading.FillFromStorage(Storage());
		}

		float shading = cachedVoxelShading.GetShading(localCornerPosition, normalSnapped);
		return shading;
	}

	private IntVector3 SnapNormal(Vector3 normal)
	{
		float x = Mathf.Abs(normal.x);
		float y = Mathf.Abs(normal.y);
		float z = Mathf.Abs(normal.z);

		if (y > x && y > z)
			return normal.y > 0 ? IntVector3.up : IntVector3.down;
		if (x > y && x > z)
			return normal.x > 0 ? IntVector3.right : IntVector3.left;
		if (z > x && z > y)
			return normal.z > 0 ? IntVector3.forward : IntVector3.back;
		return IntVector3.up;
	}

	float GetWeight(IntVector3 localCornerPosition, ref Vector3 localPosition)
	{
		float weight = 1 - Vector3.Distance(MapPositionConverter.ToFloat(localCornerPosition), localPosition);
		if (weight < SHADING_SNAP_EPSILON)
		{
			weight = 0;
		}
		else if (weight > 1 - SHADING_SNAP_EPSILON)
		{
			weight = 1;
		}
		return weight;
	}

	IntVector3 GetVoxelPosition(IntVector3 floorPosition, IntVector3 localCornerPosition, IntVector3 normal)
	{
		IntVector3 voxelPosition = floorPosition;
		if (normal.x < 0 && localCornerPosition.x == 1)
			voxelPosition.x++;
		if (normal.y < 0 && localCornerPosition.y == 1)
			voxelPosition.y++;
		if (normal.z < 0 && localCornerPosition.z == 1)
			voxelPosition.z++;

		if (normal.x > 0 && localCornerPosition.x == 0)
			voxelPosition.x--;
		if (normal.y > 0 && localCornerPosition.y == 0)
			voxelPosition.y--;
		if (normal.z > 0 && localCornerPosition.z == 0)
			voxelPosition.z--;
		return voxelPosition;
	}
}
