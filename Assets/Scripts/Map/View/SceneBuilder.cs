﻿using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(VoxelStorageComponent))]
public class SceneBuilder : BaseBehaviour, ISerializationCallbackReceiver
{
	private IVoxelStorage storage;
	private bool listenerAdded;
	private bool needReinstantiate; // only used to recreate scene after save

	public GameObject replaceFrom;
	public GameObject replaceTo;

	void Awake()
	{
		storage = GetComponent<VoxelStorageComponent>().GetStorage();

//		AddSceneListeners();
	}

#if (UNITY_EDITOR)

	public void OnBeforeSerialize()
	{
//		needReinstantiate = true;
	}

	public void OnAfterDeserialize()
	{
		//Awake();
//		needReinstantiate = true;//
	}

	void Update()
	{
		if (needReinstantiate)
		{
			Awake();
			Reinstantiate();
		}
	}
#else
	public void OnBeforeSerialize() {}
	public void OnAfterDeserialize() {}
#endif

	private void AddSceneListeners()
	{
		SceneListener.AddLoadListenerIfNotAdded(OnSceneLoad);
		SceneListener.AddSaveListenerIfNotAdded(OnSceneSave);
	}

	void OnEnable()
	{
		Debug.Log("OnEnable");
		Awake();
		AddStorageListenerIfNotYet();
		Reinstantiate();

		StaticBatchingUtility.Combine(gameObject);
	}

	void OnDisable()
	{
		Debug.Log("OnDisable");
		Awake();
//		ClearViewObjects();
		RemoveStorageListenerIfNotYet();
	}

	private void OnSceneLoad()
	{
		Debug.Log("OnSceneLoad");
//		if (enabled)
//		{
//			AddStorageListenerIfNotYet();
//			Reinstantiate();
//		}
	}

	private void OnSceneSave()
	{
		Debug.Log("OnSceneSave");
//		ClearViewObjects();
	}

	private void AddStorageListenerIfNotYet()
	{
		if (!listenerAdded)
		{
			storage.OnAdd += OnAdd;
			storage.OnAddNew += OnAddNew;
			// storage.OnRemove += OnRemove;
//			storage.OnInstantiateChunk += OnInstantiateChunk;
			storage.OnClear += OnClear;
			storage.OnMerge += OnMerge;
			storage.OnAfterChange += OnAfterChange;
			listenerAdded = true;
		}
	}

	public void RemoveStorageListenerIfNotYet()
	{
		if (listenerAdded)
		{
			storage.OnAdd -= OnAdd;
			storage.OnAddNew -= OnAddNew;
			// storage.OnRemove -= OnRemove;
//			storage.OnInstantiateChunk -= OnInstantiateChunk;
			storage.OnClear -= OnClear;
			storage.OnMerge -= OnMerge;
			storage.OnAfterChange -= OnAfterChange;
			listenerAdded = false;
		}
	}

	public void Reinstantiate()
	{
		needReinstantiate = false;
		GameObject viewObjectsContainer = ClearViewObjects();

		foreach (VoxelVerticalList verticalList in storage)
		{
			LinkedList<VoxelVerticalListChunk> chunks = verticalList.GetChunks();
			foreach (VoxelVerticalListChunk chunk in chunks)
			{
				chunk.Element.ViewObjects.Clear();
			}
		}

		int instantiated = 0;
		foreach (VoxelVerticalList verticalList in storage)
		{
			foreach (VoxelVerticalListChunk chunk in verticalList.GetChunks())
			{
				if (chunk.Element == null || chunk.Element.StoredObject == null)
				{
					Debug.Log("Instantiate null prefab at " + new Vector3(verticalList.x, chunk.Top, verticalList.z));
					continue;
				}
				for (int y = chunk.Bottom; y <= chunk.Top; y++)
				{
					var position = new IntVector3(verticalList.x, y, verticalList.z);
					GameObject viewObject = InstantiateViewObject(chunk.Element, position, viewObjectsContainer);
					chunk.Element.ViewObjects.Add(viewObject);
				}
				instantiated += chunk.Length;
			}
		}
		Debug.Log("Instantiated=" + instantiated);

		CheckViewObjects();
	}

	public bool CheckViewObjects()
	{
		int viewObjectsCount = 0;
		bool ok = true;

		foreach (VoxelVerticalList verticalList in storage)
		{
			LinkedList<VoxelVerticalListChunk> chunks = verticalList.GetChunks();
			foreach (VoxelVerticalListChunk chunk in chunks)
			{
				int viewObjects = chunk.Element.ViewObjects.Count;
				viewObjectsCount += viewObjects;

				if (viewObjects != chunk.Length)
				{
					Debug.Log("MapVoxel has different view objects count. ViewObjects=" + viewObjects + " InMapVoxels=" + chunk.Length + " ChunkTop=" + verticalList.GetPosition(chunk.Top));
					ok = false;
				}
			}
		}

		if (viewObjectsCount != GetViewObjectsContainer().transform.childCount)
		{
			Debug.Log("Different view objects count. InContainer=" + GetViewObjectsContainer().transform.childCount + " InMapVoxels=" + viewObjectsCount);
			ok = false;
		}

		Debug.Log("ok="+ok);
		return ok;
	}

	public GameObject ClearViewObjects()
	{
		DestroyImmediate(GetViewObjectsContainer());
		return GetViewObjectsContainer();
	}

	public GameObject GetViewObjectsContainer()
	{
		return transform.GetOrCreateChild("View");
	}

	private void OnAdd(IntVector3 position, int prevTop, int prevBottom, VoxelVerticalListChunk chunk)
	{
		int index = GetViewObjectIndex(position.y, prevBottom);
		GameObject viewObject = InstantiateViewObject(chunk.Element, position, GetViewObjectsContainer());
		if (index == chunk.Element.ViewObjects.Count)
		{
			chunk.Element.ViewObjects.Add(viewObject);
		}
		else if (index == -1)
		{
			chunk.Element.ViewObjects.Insert(0, viewObject);
		}
		else
		{
			DestroyViewObject(viewObject);
			throw new Exception("Unexpected ViewObject add index " + index + " position.y=" + position.y + " prevTop=" + prevTop + " viewObjects=" + chunk.Element.ViewObjects.Count);
		}

//		ViewObjectInstantiated(position);
//		CheckViewObjects();
	}

	private void OnAddNew(IntVector3 position, VoxelVerticalListChunk chunk)
	{
		GameObject viewObject = InstantiateViewObject(chunk.Element, position, GetViewObjectsContainer());
		chunk.Element.ViewObjects.Add(viewObject);

//		ViewObjectInstantiated(position);
//		CheckViewObjects();
	}

	private void OnClear(IntVector3 position, int prevTop, int prevBottom, VoxelVerticalListChunk chunk, VoxelVerticalListChunk newLowerChunk)
	{
		GameObject viewObject = null;
		int index = GetViewObjectIndex(position.y, prevBottom);
		if (newLowerChunk == null)
		{
			// reduce
			viewObject = chunk.Element.GetAndRemoveViewObject(index);
		}
		else
		{
			// split
			viewObject = chunk.Element.ViewObjects[index];
			newLowerChunk.Element.ViewObjects = chunk.Element.ViewObjects.GetRange(0, newLowerChunk.Length);
			chunk.Element.ViewObjects.RemoveRange(0, newLowerChunk.Length + 1);
		}
		DestroyViewObject(viewObject);

//		ViewObjectInstantiated(position);
//		CheckViewObjects();
	}

	private void OnMerge(IntVector3 position, VoxelVerticalListChunk chunk, VoxelVerticalListChunk nextChunk)
	{
		GameObject viewObjectsContainer = GetViewObjectsContainer();
		GameObject viewObject = InstantiateViewObject(chunk.Element, position, viewObjectsContainer);

		chunk.Element.ViewObjects.Insert(0, viewObject);
		chunk.Element.ViewObjects.InsertRange(0, nextChunk.Element.ViewObjects);

//		ViewObjectInstantiated(position);
//		CheckViewObjects();
	}

	private void OnAfterChange(IntVector3 position, VoxelVerticalListChunk chunk)
	{
		CheckViewObjects();

		ViewObjectInstantiated(position);
	}

	private int GetViewObjectIndex(int positionY, int bottom)
	{
		return positionY - bottom;
	}

	private GameObject InstantiateViewObject(MapVoxel voxel, IntVector3 position, GameObject parent = null)
	{
		GameObject viewObject = InstantiateViewObjectProp(voxel, position);
		// TODO rename -> OnJustAfterInstantiate
		gameObject.Event<SceneBuilderEvent>(e => e.OnViewObjectInstantiate(viewObject, position));

		if (parent != null)
			viewObject.transform.parent = parent.transform;

		return viewObject;
	}

	private GameObject InstantiateViewObjectProp(MapVoxel voxel, IntVector3 position)
	{
		// view up-to-down replacement (TODO move out)
		GameObject prefab = voxel.StoredObject;
		MapVoxel upperVoxel = storage.Get(position + IntVector3.up);
		if (upperVoxel != null && upperVoxel.StoredObject == replaceFrom && voxel.StoredObject == replaceFrom)
		{
			prefab = replaceTo;
		}

		Quaternion rotation = Quaternion.AngleAxis(voxel.RotationY * 90, Vector3.up);

		GameObject instantiatedObject = (GameObject)Instantiate(prefab, MapPositionConverter.ToViewPivot(position), rotation);
		instantiatedObject.TraverseChildrenWithSelf(go => go.hideFlags = HideFlags.DontSave);

		if (instantiatedObject.GetComponent<BoxCollider>() == null)
		{
			// hack for raycasting
			var boxCollider = instantiatedObject.AddComponent<BoxCollider>();
			boxCollider.center = Vector3.up * 0.5f;
			boxCollider.size = Vector3.one;
		}
		return instantiatedObject;
	}

	private void ViewObjectInstantiated(IntVector3 position)
	{
		// TODO rename -> OnAfterChange
		gameObject.Event<SceneBuilderEvent>(e => e.OnViewObjectInstantiated(position));
	}

	public GameObject ReinstantiateViewObjectAt(IntVector3 position)
	{
		LinkedListNode<VoxelVerticalListChunk> node = storage.GetVerticalListNode(position);
		if (node == null)
			return null;

		int viewObjectIndex = GetViewObjectIndex(position.y, node.Value.Bottom);

		MapVoxel mapVoxel = node.Value.Element;
		GameObject viewObject = null;
		try
		{
			viewObject = mapVoxel.ViewObjects[viewObjectIndex];
		}
		catch (ArgumentOutOfRangeException e)
		{
			Debug.Log("position=" + position + " viewObjectIndex=" + viewObjectIndex + " ViewObjects.Count=" + mapVoxel.ViewObjects.Count + " Bottom=" + node.Value.Bottom);
			viewObject = mapVoxel.ViewObjects[viewObjectIndex];
		}
		GameObject parent = viewObject.transform.parent.gameObject;

		DestroyViewObject(viewObject);

		GameObject instantiated = InstantiateViewObject(mapVoxel, position, parent);
		mapVoxel.ViewObjects[viewObjectIndex] = instantiated;
		return instantiated;
	}

	public GameObject GetViewObjectAt(IntVector3 position)
	{
		LinkedListNode<VoxelVerticalListChunk> node = storage.GetVerticalListNode(position);
		if (node == null)
			return null;

		int viewObjectIndex = GetViewObjectIndex(position.y, node.Value.Bottom);
		MapVoxel mapVoxel = node.Value.Element;
		return mapVoxel.ViewObjects[viewObjectIndex];
	}

	private void DestroyViewObject(GameObject viewObject)
	{
		DestroyImmediate(viewObject);
	}
}
