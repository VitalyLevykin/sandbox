﻿using UnityEngine;

public interface SceneBuilderEvent : BaseEvent
{
	void OnViewObjectInstantiate(GameObject viewObject, IntVector3 position);

	void OnViewObjectInstantiated(IntVector3 position);
}
