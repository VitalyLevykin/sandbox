﻿using UnityEngine;

public class VoxelAmbientShadingData
{
	public IntVector3 voxelPosition;

	readonly float[] shadings = new float[24];
	private static readonly float[] shadingCurve = { 0.0f, 0.6f, 0.8f, 1.0f };

	public void FillFromStorage(IVoxelStorage storage)
	{
		bool leftDownBack = HasShadowCaster(storage, voxelPosition + IntVector3.left + IntVector3.down + IntVector3.back);
		bool leftDown = HasShadowCaster(storage, voxelPosition + IntVector3.left + IntVector3.down);
		bool leftDownForward = HasShadowCaster(storage, voxelPosition + IntVector3.left + IntVector3.down + IntVector3.forward);
		bool leftBack = HasShadowCaster(storage, voxelPosition + IntVector3.left + IntVector3.back);
		bool left = HasShadowCaster(storage, voxelPosition + IntVector3.left);
		bool leftForward = HasShadowCaster(storage, voxelPosition + IntVector3.left + IntVector3.forward);
		bool leftUpBack = HasShadowCaster(storage, voxelPosition + IntVector3.left + IntVector3.up + IntVector3.back);
		bool leftUp = HasShadowCaster(storage, voxelPosition + IntVector3.left + IntVector3.up);
		bool leftUpForward = HasShadowCaster(storage, voxelPosition + IntVector3.left + IntVector3.up + IntVector3.forward);

		bool downBack = HasShadowCaster(storage, voxelPosition + IntVector3.down + IntVector3.back);
		bool down = HasShadowCaster(storage, voxelPosition + IntVector3.down);
		bool downForward = HasShadowCaster(storage, voxelPosition + IntVector3.down + IntVector3.forward);
		bool back = HasShadowCaster(storage, voxelPosition + IntVector3.back);
		bool forward = HasShadowCaster(storage, voxelPosition + IntVector3.forward);
		bool upBack = HasShadowCaster(storage, voxelPosition + IntVector3.up + IntVector3.back);
		bool up = HasShadowCaster(storage, voxelPosition + IntVector3.up);
		bool upForward = HasShadowCaster(storage, voxelPosition + IntVector3.up + IntVector3.forward);

		bool rightDownBack = HasShadowCaster(storage, voxelPosition + IntVector3.right + IntVector3.down + IntVector3.back);
		bool rightDown = HasShadowCaster(storage, voxelPosition + IntVector3.right + IntVector3.down);
		bool rightDownForward = HasShadowCaster(storage, voxelPosition + IntVector3.right + IntVector3.down + IntVector3.forward);
		bool rightBack = HasShadowCaster(storage, voxelPosition + IntVector3.right + IntVector3.back);
		bool right = HasShadowCaster(storage, voxelPosition + IntVector3.right);
		bool rightForward = HasShadowCaster(storage, voxelPosition + IntVector3.right + IntVector3.forward);
		bool rightUpBack = HasShadowCaster(storage, voxelPosition + IntVector3.right + IntVector3.up + IntVector3.back);
		bool rightUp = HasShadowCaster(storage, voxelPosition + IntVector3.right + IntVector3.up);
		bool rightUpForward = HasShadowCaster(storage, voxelPosition + IntVector3.right + IntVector3.up + IntVector3.forward);

		shadings[0] = GetShading(leftDown, downBack, leftDownBack, down);
		shadings[1] = GetShading(downForward, leftDown, leftDownForward, down);
		shadings[2] = GetShading(downBack, rightDown, rightDownBack, down);
		shadings[3] = GetShading(rightDown, downForward, rightDownForward, down);

		shadings[4] = GetShading(upBack, leftUp, leftUpBack, up);
		shadings[5] = GetShading(leftUp, upForward, leftUpForward, up);
		shadings[6] = GetShading(rightUp, upBack, rightUpBack, up);
		shadings[7] = GetShading(upForward, rightUp, rightUpForward, up);

		shadings[8] = GetShading(leftBack, leftDown, leftDownBack, left);
		shadings[9] = GetShading(leftDown, leftForward, leftDownForward, left);
		shadings[10] = GetShading(rightDown, rightBack, rightDownBack, right);
		shadings[11] = GetShading(rightForward, rightDown, rightDownForward, right);

		shadings[12] = GetShading(leftUp, leftBack, leftUpBack, left);
		shadings[13] = GetShading(leftForward, leftUp, leftUpForward, left);
		shadings[14] = GetShading(rightBack, rightUp, rightUpBack, right);
		shadings[15] = GetShading(rightUp, rightForward, rightUpForward, right);

		shadings[16] = GetShading(downBack, leftBack, leftDownBack, back);
		shadings[17] = GetShading(leftForward, downForward, leftDownForward, forward);
		shadings[18] = GetShading(rightBack, downBack, rightDownBack, back);
		shadings[19] = GetShading(downForward, rightForward, rightDownForward, forward);

		shadings[20] = GetShading(leftBack, upBack, leftUpBack, back);
		shadings[21] = GetShading(upForward, leftForward, leftUpForward, forward);
		shadings[22] = GetShading(upBack, rightBack, rightUpBack, back);
		shadings[23] = GetShading(rightForward, upForward, rightUpForward, forward);
	}

	private bool HasShadowCaster(IVoxelStorage storage, IntVector3 position)
	{
		MapVoxel voxel = storage.Get(position);
		if (voxel != null)
		{
			var prop = voxel.StoredObject.GetComponent<PropComponent>();
			if (prop != null)
				return prop.castAmbientShadow;
			Debug.LogWarning("StoredObject does not have PropComponent " + voxel.StoredObject);
		}
		return false;
	}

	public float GetShading(IntVector3 localCornerPosition, IntVector3 normalSnapped)
	{
		return shadings[GetIndex(localCornerPosition, normalSnapped)];
	}

	int GetIndex(IntVector3 localCornerPosition, IntVector3 normalSnapped)
	{
		int normalIndex = 0;
		if (normalSnapped.x != 0)
			normalIndex = 1;
		else if (normalSnapped.z != 0)
			normalIndex = 2;
		return 8*normalIndex + 4*localCornerPosition.y + 2*localCornerPosition.x + localCornerPosition.z;
	}

	static float GetShading(bool side1, bool side2, bool corner, bool covered)
	{
		int type = GetShadingType(side1, side2, corner, covered);
		return shadingCurve[type];
	}

	static int GetShadingType(bool side1, bool side2, bool corner, bool covered)
	{
		if (side1 && side2 || covered)
			return 3;
		int result = 0;
		if (side1)
			result++;
		if (side2)
			result++;
		if (corner)
			result++;
		return result;
	}
}
