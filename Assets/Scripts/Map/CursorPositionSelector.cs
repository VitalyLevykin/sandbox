﻿using UnityEngine;

public class CursorPositionSelector
{
	private const float OFFSET = 0.01f;

	public bool fixHeight;
	public int height;
	public bool insideOrOutside;

	public IntVector3? GetByWorldRay(Ray ray)
	{
		if (fixHeight)
		{
			return RaycastHorizPlaneToInt(ray, height, insideOrOutside);
		}
		else
		{
			return RaycastToInt(ray, insideOrOutside);
		}
	}

	public static IntVector3? RaycastHorizPlaneToInt(Ray ray, float planeHeight, bool insideOrOutside)
	{
		Vector3 point;
		Vector3 normal;
		if (IntersectRayWithHorizPlane(ray, planeHeight, out point, out normal))
		{
			ApplyOffset(ref point, normal, insideOrOutside);
			return MapPositionConverter.FloorToInt(point);
		}
		return null;
	}

	public static void ApplyOffset(ref Vector3 point, Vector3 normal, bool insideOrOutside)
	{
		if (insideOrOutside)
			point -= normal * OFFSET;
		else
			point += normal * OFFSET;
	}
	
	public static IntVector3? RaycastToInt(Ray ray, bool insideOrOutside)
	{
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit))
		{
			Vector3 point = hit.point;
			ApplyOffset(ref point, hit.normal, insideOrOutside);
			return MapPositionConverter.FloorToInt(point);
		}
		return null;
	}
	
	public static bool RaycastToInt(Ray ray, out IntVector3 insidePosition, out IntVector3 outsidePosition)
	{
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit))
		{
			Vector3 point = hit.point;
			Vector3 inside = point;
			Vector3 outside = point;
			ApplyOffset(ref inside, hit.normal, true);
			ApplyOffset(ref outside, hit.normal, false);
			insidePosition = MapPositionConverter.FloorToInt(inside);
			outsidePosition = MapPositionConverter.FloorToInt(outside);
			return true;
		}
		insidePosition = IntVector3.zero;
		outsidePosition = IntVector3.zero;
		return false;
	}

	public static bool IntersectRayWithHorizPlane(Ray ray, float planeHeight, out Vector3 intersectPoint, out Vector3 normal)
	{
		intersectPoint = Vector3.zero;
		normal = ray.origin.y >= planeHeight ? Vector3.up : Vector3.down;

		if (ray.origin.y <= planeHeight && ray.direction.y <= 0)
			return false;
		if (ray.origin.y >= planeHeight && ray.direction.y >= 0)
			return false;

		float dy = planeHeight - ray.origin.y;
		float t = dy / ray.direction.y;
		float dx = t * ray.direction.x;
		float dz = t * ray.direction.z;

		intersectPoint = ray.origin + new Vector3(dx, dy, dz);
		return true;
	}
}
