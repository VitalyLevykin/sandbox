using UnityEngine;
//using UnityEditor;

public class DrawUtils
{
	public const float BLOCK_SIZE = 1;

	public static void DrawAABoxFrame(Vector3 pos, float blockSize)
	{
		DrawAABoxFrame(pos, new Vector3(blockSize, blockSize, blockSize));
	}

	public static void DrawAABoxFrame(IntVector3 pos)
	{
		DrawAABoxFrame(MapPositionConverter.ToFloat(pos), 1);
	}

	public static void DrawAABoxFrame(IntVector3 pos1, IntVector3 pos2)
	{
		Vector3 pos1min = MapPositionConverter.ToFloat(pos1);
		Vector3 pos1max = pos1min + Vector3.one * BLOCK_SIZE;
		Vector3 pos2min = MapPositionConverter.ToFloat(pos2);
		Vector3 pos2max = pos2min + Vector3.one * BLOCK_SIZE;

		Bounds bounds = new Bounds(pos1min, Vector3.zero);
		bounds.Encapsulate(pos1max);
		bounds.Encapsulate(pos2min);
		bounds.Encapsulate(pos2max);

		DrawAABoxFrame(bounds);
	}

	public static void DrawAABoxFrame(Bounds bounds)
	{
		DrawAABoxFrame(bounds.min, bounds.size);
	}

	public static void DrawAABoxFrame(Vector3 pos, Vector3 size)
	{
		DrawAAGridXZ(pos, size.x, size.z, 1, 1);
		DrawAAGridXZ(new Vector3(pos.x, pos.y + size.y, pos.z), size.x, size.z, 1, 1);

//		Handles.DrawLine(pos, new Vector3(pos.x, pos.y + size.y, pos.z));
//		Handles.DrawLine(new Vector3(pos.x + size.x, pos.y, pos.z), new Vector3(pos.x + size.x, pos.y + size.y, pos.z));
//		Handles.DrawLine(new Vector3(pos.x, pos.y, pos.z + size.z), new Vector3(pos.x, pos.y + size.y, pos.z + size.z));
//		Handles.DrawLine(new Vector3(pos.x + size.x, pos.y, pos.z + size.z), new Vector3(pos.x + size.x, pos.y + size.y, pos.z + size.z));
		Debug.DrawLine(pos, new Vector3(pos.x, pos.y + size.y, pos.z));
		Debug.DrawLine(new Vector3(pos.x + size.x, pos.y, pos.z), new Vector3(pos.x + size.x, pos.y + size.y, pos.z));
		Debug.DrawLine(new Vector3(pos.x, pos.y, pos.z + size.z), new Vector3(pos.x, pos.y + size.y, pos.z + size.z));
		Debug.DrawLine(new Vector3(pos.x + size.x, pos.y, pos.z + size.z), new Vector3(pos.x + size.x, pos.y + size.y, pos.z + size.z));
	}
	
	public static void DrawAAGridXZ(Vector3 pos, float blockSizeX, float blockSizeZ, int xLines, int zLines)
	{
		for (int i = 0; i <= xLines; i++)
		{
			float xOffset = blockSizeX * i;
			var from = new Vector3(pos.x + xOffset, pos.y, pos.z);
			var to = new Vector3(pos.x + xOffset, pos.y, pos.z + zLines * blockSizeZ);
//			Handles.DrawLine(from, to);
			Debug.DrawLine(from, to);
		}
		
		for (int i = 0; i <= zLines; i++)
		{
			float zOffset = blockSizeZ * i;
			var from = new Vector3(pos.x, pos.y, pos.z + zOffset);
			var to = new Vector3(pos.x + xLines * blockSizeX, pos.y, pos.z + zOffset);
//			Handles.DrawLine(from, to);
			Debug.DrawLine(from, to);
		}
	}
}
