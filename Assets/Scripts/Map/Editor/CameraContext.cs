using System;

public class CameraContext
{
	private readonly UnityEngine.Camera _camera;
	private readonly Action _action;
	
	private CameraContext(UnityEngine.Camera camera, Action action)
	{
		_camera = camera;
		_action = action;
	}
	
	public static void InvokeWithCamera(UnityEngine.Camera camera, Action action)
	{
		//			Contract.Requires<ArgumentNullException>(camera != null);
		//			Contract.Requires<ArgumentNullException>(action != null);
		
		UnityEngine.Camera prevCamera = UnityEngine.Camera.current;
		UnityEngine.Camera.SetupCurrent(camera);
		try
		{
			action();
		}
		finally
		{
			UnityEngine.Camera.SetupCurrent(prevCamera);
		}
	}
	
	public static Action WrapAction(UnityEngine.Camera camera, Action action)
	{
		CameraContext cameraContext = new CameraContext(camera, action);
		return cameraContext.Invoke;
	}
	
	private void Invoke()
	{
		InvokeWithCamera(_camera, _action);
	}
}
