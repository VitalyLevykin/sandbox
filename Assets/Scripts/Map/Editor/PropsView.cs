﻿using UnityEditor;
using UnityEngine;

public class PropsView : EditorWindow
{
	[MenuItem("Window/Props")]
	public static void ShowWindow()
	{
		GetWindow(typeof (PropsView)).Show();
	}

	void OnSelectionChange()
	{
		var levelEditor = GetWindow<LevelEditor>(null, false);
		if (IsPrefab(Selection.activeObject))
		{
			levelEditor.clearing = false;
			levelEditor.Replacing = false;
		}
		Repaint();
	}

	void OnGUI()
	{
		var levelEditor = GetWindow<LevelEditor>(null, false);

		EditorGUILayout.BeginVertical();

		levelEditor.clearing = EditorGUILayout.Toggle("CLEAR", levelEditor.clearing);
		levelEditor.rectMode = EditorGUILayout.Toggle("RECT", levelEditor.rectMode);
		levelEditor.Replacing = EditorGUILayout.Toggle("REPLACING", levelEditor.Replacing);

		if (levelEditor.clearing)
		{
			Selection.activeObject = null;
			levelEditor.Replacing = true;
		}

		EditorGUILayout.EndVertical();
	}

	private bool IsPrefab(Object unityObject)
	{
		return unityObject is GameObject && !string.IsNullOrEmpty(AssetDatabase.GetAssetPath(unityObject));
	}
}
