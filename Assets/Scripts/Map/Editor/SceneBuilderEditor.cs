﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof (SceneBuilder))]
public class SceneBuilderEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		var sceneBuilder = (SceneBuilder) target;

		if (GUILayout.Button("Reinstantiate"))
		{
			sceneBuilder.Reinstantiate();
		}

		if (GUILayout.Button("Check view objects"))
		{
			if (sceneBuilder.CheckViewObjects())
				Debug.Log("CheckViewObjects OK");
		}
	}
}
