﻿public interface UndoAction
{
	void Undo();

	void Redo();
}
