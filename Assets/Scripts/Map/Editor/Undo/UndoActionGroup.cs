﻿using System.Collections.Generic;

public class UndoActionGroup : List<UndoAction>
{
	public void Undo()
	{
		foreach (UndoAction undoAction in this)
		{
			undoAction.Undo();
		}
	}

	public void Redo()
	{
		foreach (UndoAction undoAction in this)
		{
			undoAction.Redo();
		}
	}
}
