﻿using UnityEngine;

public class MapVoxelUndoAction : UndoAction
{
	private IntVector3 position;
	private MapVoxelData prevData;
	private MapVoxelData data;

	private static IVoxelStorage voxelStorage;

	public MapVoxelUndoAction()
	{
	}

	public MapVoxelUndoAction(IntVector3 position, MapVoxelData prevData, MapVoxelData data)
	{
		this.position = position;
		this.prevData = prevData;
		this.data = data;

		if (Equals(prevData, data))
			Debug.LogWarning("Equal data: prevData=" + prevData + " data=" + data);
	}

	public void Undo()
	{
		Do(prevData);
	}

	public void Redo()
	{
		Do(data);
	}

	private void Do(MapVoxelData data)
	{
		if (data == null)
			GetStorage().ClearElement(position);
		else
			GetStorage().Set(position, new MapVoxel().Init(data.storedObject, data.rotationY, data.split));
	}

	private IVoxelStorage GetStorage()
	{
		return voxelStorage ?? (voxelStorage = Object.FindObjectOfType<VoxelStorageComponent>().GetStorage());
	}
}
