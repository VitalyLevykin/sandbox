﻿using System.Collections.Generic;
using UnityEngine;

public class LevelEditorUndo
{
	private List<UndoActionGroup> actionGroups = new List<UndoActionGroup>();
	private int currentIndex = -1;
	private UndoActionGroup collectingGroup;

	public void BeginActionGroup()
	{
		if (collectingGroup != null)
			Debug.LogWarning("Dropping unfinished undo group");

		collectingGroup = new UndoActionGroup();
	}

	public void EndActionGroup()
	{
		if (collectingGroup == null)
		{
			Debug.LogWarning("No action group");
			return;
		}

		AddActionGroup(collectingGroup);
		collectingGroup = null;
	}

	public void AddAction(UndoAction action)
	{
		if (collectingGroup == null)
		{
			Debug.LogWarning("No action group");
			return;
		}

		collectingGroup.Add(action);
	}

	public void AddSeparateAction(UndoAction action)
	{
		BeginActionGroup();
		AddAction(action);
		EndActionGroup();
	}

	public void AddActionGroup(UndoActionGroup actionGroup)
	{
		if (actionGroup.Count >= 100)
			Debug.LogWarning("AddActionGroup many actions " + actionGroup.Count);

		RemoveRedoGroups();

		if (actionGroups.Count >= 100) // max undo count
		{
			actionGroups.RemoveAt(0);
			currentIndex--;
		}

		actionGroups.Add(actionGroup);
		currentIndex++;
	}

	public void RemoveRedoGroups()
	{
		if (currentIndex < actionGroups.Count - 1)
		{
			int startIndex = currentIndex + 1;
			int count = actionGroups.Count - currentIndex - 1;
			actionGroups.RemoveRange(startIndex, count);
		}
	}

	public void PerformUndo()
	{
		if (currentIndex >= 0)
		{
			actionGroups[currentIndex].Undo();
			currentIndex--;
		}
	}

	public void PerformRedo()
	{
		if (currentIndex < actionGroups.Count - 1)
		{
			actionGroups[currentIndex + 1].Redo();
			currentIndex++;
		}
	}

	public void Stats()
	{
		Debug.Log("Stats currentIndex=" + currentIndex + " actionGroups.Count=" + actionGroups.Count);
	}
}
