using UnityEngine;
using System;
using UnityEditor;
using System.Globalization;

	public class PrefColor : IPrefType
		{
				private string m_name;
				private Color m_color;
				private Color m_DefaultColor;
		
				public Color Color {
						get {
								return this.m_color;
						}
						set {
								this.m_color = value;
						}
				}
		
				public string Name {
						get {
								return this.m_name;
						}
				}
		
				public PrefColor ()
				{
				}
		
				public PrefColor (string name, float defaultRed, float defaultGreen, float defaultBlue, float defaultAlpha)
				{
						this.m_name = name;
						this.m_color = this.m_DefaultColor = new Color (defaultRed, defaultGreen, defaultBlue, defaultAlpha);
						PrefColor prefColor = SettingsGet<PrefColor> (name, this);
						this.m_name = prefColor.Name;
						this.m_color = prefColor.Color;
				}
		
				public static implicit operator Color (PrefColor pcolor)
				{
						return pcolor.Color;
				}

				private static T SettingsGet<T> (string name, T defaultValue) where T : IPrefType, new()
				{
						if ((object)defaultValue == null)
								throw new ArgumentException ("default can not be null", "defaultValue");
						//if (Settings.m_Prefs.ContainsKey(name))
						//	return (T) Settings.m_Prefs[name];
						string @string = EditorPrefs.GetString (name, string.Empty);
						if (@string == string.Empty) {
//				Settings.Set<T>(name, defaultValue);
								return defaultValue;
						} else {
								defaultValue.FromUniqueString (@string);
//				Settings.Set<T>(name, defaultValue);
								return defaultValue;
						}
				}

		public string ToUniqueString()
		{
			CultureInfo invariantCulture = CultureInfo.InvariantCulture;
			string format = "{0};{1};{2};{3};{4}";
			object[] objArray = new object[5];
			string str = this.m_name;
			objArray[0] = (object) str;
			objArray[1] = (object) (ValueType) Color.r;
			objArray[2] = (object) (ValueType) this.Color.g;
			objArray[3] = (object) (ValueType) this.Color.b;
			objArray[4] = (object) (ValueType) this.Color.a;
			return string.Format((IFormatProvider) invariantCulture, format, objArray);
		}
		
		public void FromUniqueString(string s)
		{
			string str = s;
			char[] chArray = new char[1];
			int index = 0;
			int num = 59;
			chArray[index] = (char) num;
			string[] strArray = str.Split(chArray);
			if (strArray.Length != 5)
			{
				Debug.LogError((object) "Parsing PrefColor failed");
			}
			else
			{
				this.m_name = strArray[0];
				strArray[1] = strArray[1].Replace(',', '.');
				strArray[2] = strArray[2].Replace(',', '.');
				strArray[3] = strArray[3].Replace(',', '.');
				strArray[4] = strArray[4].Replace(',', '.');
				float result1;
				float result2;
				float result3;
				float result4;
				if (float.TryParse(strArray[1], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture.NumberFormat, out result1) & float.TryParse(strArray[2], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture.NumberFormat, out result2) & float.TryParse(strArray[3], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture.NumberFormat, out result3) & float.TryParse(strArray[4], NumberStyles.Float, (IFormatProvider) CultureInfo.InvariantCulture.NumberFormat, out result4))
					this.m_color = new Color(result1, result2, result3, result4);
				else
					Debug.LogError((object) "Parsing PrefColor failed");
			}
		}
		
		internal void ResetToDefault()
		{
			this.m_color = this.m_DefaultColor;
		}
		}

