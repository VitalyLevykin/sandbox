﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(VoxelStorageEditableComponent))]
public class VoxelStorageComponentEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		var storageComponent = ((VoxelStorageEditableComponent)target).GetComponent<VoxelStorageComponent>();

		if (GUILayout.Button("Stats"))
		{
			storageComponent.Stats();
		}

		if (GUILayout.Button("Dump"))
		{
			storageComponent.Dump();
		}

		if (GUILayout.Button("Perlin Noise"))
		{
			storageComponent.PerlinNoise();
		}

		if (GUILayout.Button("Batch static"))
		{
			StaticBatchingUtility.Combine(storageComponent.gameObject);
		}
	}
}
