﻿using UnityEditor;

[InitializeOnLoad]
public class EditorSceneListener : AssetModificationProcessor
{
	private static string loadedLevel;

	static EditorSceneListener()
	{
		loadedLevel = EditorApplication.currentScene;
		EditorApplication.hierarchyWindowChanged += OnHierarchyChange;
	}

	private static void OnHierarchyChange()
	{
		if (EditorApplication.currentScene != loadedLevel)
		{
			loadedLevel = EditorApplication.currentScene;

			SceneListener.SendLoadEvent();
		}
	}

	private static void OnWillSaveAssets(string[] param)
	{
		foreach (string p in param)
		{
			if (p.Contains(EditorApplication.currentScene))
			{
				SceneListener.SendSaveEvent();
			}
		}
	}
}
