﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using System;
using Object = UnityEngine.Object;

[Serializable]
[InitializeOnLoad]
public class LevelEditor : SearchableEditorWindow
{
	private static PrefColor kSceneViewBackground = new PrefColor("Scene/Background", 0.278431f, 0.278431f, 0.278431f, 0.0f);
	internal static Color kSceneViewFrontLight = new Color(0.769f, 0.769f, 0.769f, 1f);
	internal static Color kSceneViewUpLight = new Color(0.212f, 0.227f, 0.259f, 1f);
	internal static Color kSceneViewMidLight = new Color(57.0f / 500.0f, 0.125f, 0.133f, 1f);
	internal static Color kSceneViewDownLight = new Color(0.047f, 0.043f, 0.035f, 1f);
	[NonSerialized]
	private static readonly Quaternion kDefaultRotation = Quaternion.LookRotation(new Vector3(-1f, -0.7f, -1f));
	[NonSerialized]
	private static readonly float kDefaultViewSize = 10f;
	[NonSerialized]
	private static readonly Vector3 kDefaultPivot = Vector3.zero;

	[SerializeField]
	private Vector3 m_Position = LevelEditor.kDefaultPivot;
	[SerializeField]
	private Quaternion m_Rotation = LevelEditor.kDefaultRotation;
	[SerializeField]
	private float m_Size = LevelEditor.kDefaultViewSize;

	[NonSerialized]
	private Camera m_Camera;
	[NonSerialized]
	private Light[] m_Light = new Light[3];
	[SerializeField]
	private Shader m_ReplacementShader;
	[SerializeField]
	private string m_ReplacementString;

	[SerializeField]
	public bool clearing;

	[SerializeField]
	public bool rectMode;
	private IntVector3 rectBeginPos;

	private double m_lastRenderedTime;

	private bool _cursorBlockVisible;
	private Vector2 mousePos;
	private IntVector3 _cursorBlockPos;

	private bool drawing;
	private int undoIndex;
	private bool undoAdded;
//	private int currentHeightDrawing;
	private LevelEditorUndo undo = new LevelEditorUndo();
	private HashSet<IntVector3> processedPositions = new HashSet<IntVector3>();

	private CursorPositionSelector cursorPositionSelector = new CursorPositionSelector();

	private VoxelStorageComponent storageComponent;

	static LevelEditor()
	{
		//		SceneView.s_LastCursor = MouseCursor.Arrow;
		//		SceneView.s_MouseRects = new List<SceneView.CursorRect>();
		//		SceneView.s_SceneViews = new ArrayList();
		//		SceneView.s_Fx = new GUIContent("Effects");
		//		SceneView.s_Lighting = EditorGUIUtility.IconContent("SceneviewLighting");
		//		SceneView.s_AudioPlay = EditorGUIUtility.IconContent("SceneviewAudio");
		//		SceneView.s_GizmosContent = new GUIContent("Gizmos");
		//		SceneView.s_2DMode = new GUIContent("2D");asdsd
	}

	public LevelEditor()
	{
		//		m_HierarchyType = HierarchyType.GameObjects;
		depthBufferBits = 32;
		antiAlias = -1;
	}

	[MenuItem("Window/Level Editor %L")]
	public static void ShowWindow()
	{
		Debug.Log("ShowWindow");
		var levelEditor = GetWindow<LevelEditor>("Level Editor", typeof(SceneView));
		levelEditor.Show();
	}

	[MenuItem("Edit/Level Editor Undo Stats %E")]
	public static void Stats()
	{
		var levelEditor = GetWindow<LevelEditor>("Level Editor", false);
		levelEditor.undo.Stats();
	}

	[MenuItem("Edit/Level Editor Undo %U")]
	public static void Undo()
	{
		var levelEditor = GetWindow<LevelEditor>("Level Editor", false);
		levelEditor.undo.PerformUndo();
	}

	[MenuItem("Edit/Level Editor Redo %R")]
	public static void Redo()
	{
		var levelEditor = GetWindow<LevelEditor>("Level Editor", false);
		levelEditor.undo.PerformRedo();
	}

	public override void OnEnable()
	{
		base.OnEnable();
		autoRepaintOnSceneChange = true;
		wantsMouseMove = true;
		//EditorApplication.modifierKeysChanged += new EditorApplication.CallbackFunction(SceneView.RepaintAll);

//		sceneBuilder.AddStorageListenerIfNotYet();
	}

	public override void OnDisable()
	{
//		sceneBuilder.RemoveStorageListenerIfNotYet();

		//EditorApplication.modifierKeysChanged -= new EditorApplication.CallbackFunction(SceneView.RepaintAll);
		if (m_Camera)
			DestroyImmediate(m_Camera.gameObject, true);
		if (m_Light[0])
			DestroyImmediate(m_Light[0].gameObject, true);
		if (m_Light[1])
			DestroyImmediate(m_Light[1].gameObject, true);
		if (m_Light[2])
			DestroyImmediate(m_Light[2].gameObject, true);
		base.OnDisable();
	}

	public void OnDestroy()
	{
	}

	public void Update()
	{
		if (m_lastRenderedTime + 0.0329999998211861 >= EditorApplication.timeSinceStartup)
			return;

		var sceneView = SceneView.lastActiveSceneView;
		if (sceneView != null)
		{
			//m_Position = sceneView.pivot;
			m_Rotation = sceneView.rotation;
			m_Size = sceneView.size;
			//			m_Camera.fieldOfView = sceneView.camera.fieldOfView;
			//m_Camera.rect = sceneView.camera.rect;
		}
//		sceneBuilder.AddStorageListenerIfNotYet();

		m_lastRenderedTime = EditorApplication.timeSinceStartup;
		Repaint();
	}

	private void DrawCursorBlock()
	{
		if (_cursorBlockVisible)
		{
			///////
			Handles.color = Color.white;
			IVoxelStorage storage = GetStorage();
			if (storage != null && IsValidPosition(storage, _cursorBlockPos) && storage.Has(_cursorBlockPos))
			{
				Handles.color = Color.red;
			}
			///////
////			Ray ray = GeometryUtils.GUIPointToWorldRay(mousePos, Camera.current);
//			Ray ray = HandleUtility.GUIPointToWorldRay(mousePos);
////			Ray ray = Camera.current.ScreenPointToRay(new Vector3(mousePos.x, Screen.height - mousePos.y, 0));
//			RaycastHit hit;
//			if (Physics.Raycast(ray, out hit))
//			{
////				Handles.color = Color.red;
//				Handles.DrawLine(new Vector3(0, 0, 0), hit.point);
//			}

			if (rectMode && drawing)
			{
				DrawUtils.DrawAABoxFrame(_cursorBlockPos, rectBeginPos);
			}
			else
				DrawUtils.DrawAABoxFrame(_cursorBlockPos);
		}
	}

	private bool IsValidPosition(IVoxelStorage storage, IntVector3 position)
	{
		IntVector3 minPosition = storage.GetMinPosition();
		IntVector3 maxPosition = storage.GetMaxPosition();

		return position.x >= minPosition.x && position.y >= minPosition.y && position.z >= minPosition.z
			&& position.x <= maxPosition.x && position.y <= maxPosition.y && position.z <= maxPosition.z;
	}

	public bool Replacing
	{
		get { return cursorPositionSelector.insideOrOutside; }
		set { cursorPositionSelector.insideOrOutside = value; }
	}

	// TODO OnRepaint?
	private void OnGUI()
	{
		SetupCamera();
		RenderingPath renderingPath = m_Camera.renderingPath;
		Rect position1 = new Rect(0.0f, 19.0f, position.width, position.height /*- 17f*/);

		float verticalFov = GetVerticalFOV(90f);
		float fieldOfView = m_Camera.fieldOfView;
		m_Camera.fieldOfView = verticalFov;
		Handles.ClearCamera(position1, m_Camera);
		m_Camera.fieldOfView = fieldOfView;
		m_Camera.cullingMask = Tools.visibleLayers;
		Handles.SetCamera(position1, m_Camera);

//		sceneBuilder.AddStorageListenerIfNotYet();

		// level drawing
		if (Event.current.button == 0 && (Event.current.type == EventType.MouseDown || Event.current.type == EventType.MouseDrag || Event.current.type == EventType.MouseUp))
		{
			IntVector3? intPos = cursorPositionSelector.GetByWorldRay(HandleUtility.GUIPointToWorldRay(Event.current.mousePosition));
			if (intPos.HasValue && Event.current.type == EventType.MouseDown)
			{
				Debug.Log("Click " + intPos);
				cursorPositionSelector.fixHeight = true;
				cursorPositionSelector.height = intPos.Value.y;
				drawing = true;

				if (rectMode)
				{
					rectBeginPos = intPos.Value;
				}
			}

			if (drawing && intPos.HasValue && (Event.current.type == EventType.MouseDown || Event.current.type == EventType.MouseDrag))
			{
				if (rectMode)
				{
					IntVector3 pos = intPos.Value;
					IntVector3 min = IntVector3.Min(rectBeginPos, pos);
					IntVector3 max = IntVector3.Max(rectBeginPos, pos);
					for (int x = min.x; x <= max.x; x++)
					{
						for (int z = min.z; z <= max.z; z++)
						{
							InstantiateIfEmptyAndSelected(new IntVector3(x, pos.y, z));
						}
					}
				}
				else
					InstantiateIfEmptyAndSelected(intPos.Value);
			}

			if (Event.current.type == EventType.MouseUp)
			{
				cursorPositionSelector.fixHeight = false;
				drawing = false;
				if (undoAdded)
				{
					undoAdded = false;
//					Undo.CollapseUndoOperations(undoIndex);
					undo.EndActionGroup();
				}
				processedPositions.Clear();
			}
		}
//		if (Event.current.type == EventType.MouseDown)
//		{
//			if (Event.current.button == 0)
//			{
//				//drawing = true;
//
//				IntVector3 intPos;
//				if (cursorPositionSelector.GetByGUIPoint(Event.current.mousePosition, out intPos))
//				{
//					cursorPositionSelector.fixHeight = true;
//					cursorPositionSelector.height = intPos.y;
//				}
//			}
//		}
//		if ((Event.current.type == EventType.MouseDown || Event.current.type == EventType.MouseDrag) && Event.current.button == 0)
//		{
//			IntVector3 intPos;
//			if (GUIPointToGridPosition(Event.current.mousePosition, !clearing, currentHeightDrawing, out intPos))
//			{
//				if (intPos.y == currentHeightDrawing)
//				{
//					InstantiateIfEmptyAndSelected(intPos);
//				}
//			}
//		}
//		if (Event.current.type == EventType.MouseUp)
//		{
//			drawing = false;
//		}

		// camera move
		if (Event.current.type == EventType.MouseDrag)
		{
			if (Event.current.button == 1)
			{
				Quaternion rotation = SceneView.lastActiveSceneView.rotation;
				Quaternion quaternion1 = Quaternion.AngleAxis((float)((double)Event.current.delta.y * (3.0 / 1000.0) * 57.2957801818848), rotation * Vector3.right) * rotation;
				Quaternion quaternion2 = Quaternion.AngleAxis((float)((double)Event.current.delta.x * (3.0 / 1000.0) * 57.2957801818848), Vector3.up) * quaternion1;
				SceneView.lastActiveSceneView.rotation = quaternion2;
			}
			if (Event.current.button == 2)
			{
				Vector2 oldScreenPos = Event.current.mousePosition - Event.current.delta;
				Vector2 newScreenPos = Event.current.mousePosition;

				HorizCameraController cameraController = new HorizCameraController(0, 1);
				Vector3 pointDelta = cameraController.CalcMoveDelta(m_Camera, oldScreenPos, newScreenPos);

				m_Position -= pointDelta;

				//				EditorGUIUtility.AddCursorRect(position, MouseCursor.Pan);
			}
			UpdateCursorBox(Event.current.mousePosition);
		}
		else
		{
			//			EditorGUIUtility.AddCursorRect(position, MouseCursor.Arrow);
		}

		// camera height
		if (Event.current.type == EventType.ScrollWheel)
		{
			m_Position.y += Event.current.delta.y;
		}

		// cursor update
		if (Event.current.type == EventType.MouseMove)
		{
			mousePos = Event.current.mousePosition;
			if (UpdateCursorBox(Event.current.mousePosition))
			{
				//Repaint();
			}
		}
		if (Event.current.type == EventType.KeyDown)
		{
			if (Event.current.keyCode == KeyCode.PageDown)
				cursorPositionSelector.height--;
			if (Event.current.keyCode == KeyCode.PageUp)
				cursorPositionSelector.height++;
		}

		if (Event.current.type == EventType.Repaint)
		{
			m_Camera.SetReplacementShader(m_ReplacementShader, m_ReplacementString);
		}

		// TODO не каждый event должен приводить к перерисовке (dirty flag)
		DrawCameraMode m_RenderMode = DrawCameraMode.Normal;
		DrawCameraImpl(position1, m_Camera, m_RenderMode, true /*, gridParam*/, false);

		DrawCursorBlock();
	}

	//	private GameObject InstantiateIfEmptyAndHitAndSelected(Vector2 guiPoint)
	//	{
	//		IntVector3 position;
	//		if (GUIPointToGridPosition(guiPoint, out position))
	//		{
	//			return InstantiateIfEmptyAndSelected(position);
	//		}
	//		return null;
	//	}

	private void InstantiateIfEmptyAndSelected(IntVector3 position)
	{
		VoxelStorageComponent storageComponent = GetStorageComponent();
		if (storageComponent == null)
			return;

		IVoxelStorage storage = GetStorage();

		if (!IsValidPosition(storage, position))
			return;

		if (processedPositions.Contains(position))
			return;

		if (clearing)
		{
			MapVoxel mapVoxel = storage.Get(position);
			if (mapVoxel != null)
			{
				Debug.Log("Clear some");
//				Undo.RecordObject(storageComponent, "Clear blocks");
				if (!undoAdded)
				{
//					undoIndex = Undo.GetCurrentGroup();
					undoAdded = true;
					undo.BeginActionGroup();
				}
				undo.AddAction(new MapVoxelUndoAction(position, mapVoxel.Data, null));
				storage.ClearElement(position);
				EditorUtility.SetDirty(storageComponent);
			}
		}
		else if (IsPrefab(Selection.activeObject))
		{
//			Undo.RecordObject(storageComponent, "Instantiate blocks");
			MapVoxel mapVoxel = storage.Get(position);
			if (!undoAdded)
			{
//				undoIndex = Undo.GetCurrentGroup();
				undoAdded = true;
				undo.BeginActionGroup();
			}
//			if (!undoAdded)
//			{
//				Undo.RecordObject(storageComponent, "Instantiate block");
//				undoAdded = true;
//			}
			MapVoxel newMapVoxel = new MapVoxel().Init((GameObject) Selection.activeObject, 0, false);
			undo.AddAction(new MapVoxelUndoAction(position, mapVoxel != null ? mapVoxel.Data : null, newMapVoxel.Data));
			storage.Set(position, newMapVoxel);
			EditorUtility.SetDirty(storageComponent);
		}
		processedPositions.Add(position);
	}

	private bool IsPrefab(Object unityObject)
	{
		return unityObject is GameObject && !string.IsNullOrEmpty(AssetDatabase.GetAssetPath(unityObject));
	}

	private void SetupCamera()
	{
		if (!m_Camera)
			Setup();

		m_Camera.backgroundColor = (Color)LevelEditor.kSceneViewBackground;
		//EditorUtility.SetCameraAnimateMaterials(m_Camera, true);
		ResetIfNaN();
		m_Camera.transform.rotation = (Quaternion)m_Rotation;

		float aspectNeutralFOV = 90; //m_Ortho.Fade(90f, 0.0f);
		if ((double)aspectNeutralFOV > 3.0)
		{
			m_Camera.orthographic = false;
			m_Camera.fieldOfView = GetVerticalFOV(aspectNeutralFOV);
		}
		else
		{
			m_Camera.orthographic = true;
			m_Camera.orthographicSize = GetVerticalOrthoSize();
		}
		m_Camera.transform.position = m_Position + m_Camera.transform.rotation * new Vector3(0.0f, 0.0f, -cameraDistance);
		if (m_Size < 1)
		{
			m_Camera.nearClipPlane = 0.005f;
			m_Camera.farClipPlane = 1000f;
		}
		else if (m_Size < 100)
		{
			m_Camera.nearClipPlane = 0.03f;
			m_Camera.farClipPlane = 3000f;
		}
		else if (m_Size < 1000)
		{
			m_Camera.nearClipPlane = 0.5f;
			m_Camera.farClipPlane = 20000f;
		}
		else
		{
			m_Camera.nearClipPlane = 1f;
			m_Camera.farClipPlane = 1000000f;
		}
		m_Camera.renderingPath = LevelEditor.GetSceneViewRenderingPath();
		//Handles.EnableCameraFlares(m_Camera, true);//m_SceneViewState.showFlares);
		//Handles.EnableCameraSkybox(m_Camera, true);//m_SceneViewState.showSkybox);
		m_Light[0].transform.position = m_Camera.transform.position;
		m_Light[0].transform.rotation = m_Camera.transform.rotation;
	}

	private void Setup()
	{
		GameObject cameraObject = EditorUtility.CreateGameObjectWithHideFlags("SceneCamera", HideFlags.HideAndDontSave, new Type[] { typeof(Camera) });
		cameraObject.AddComponent<FlareLayer>();
		//UnityEngineInternal.APIUpdaterRuntimeServices.AddComponent(cameraObject, "Assets/Scripts/LevelEditor/LevelEditor.cs (339,3)", "HaloLayer");
		m_Camera = cameraObject.GetComponent<Camera>();
		m_Camera.enabled = false;
		for (int i = 0; i < 3; ++i)
		{
			GameObject objectWithHideFlags2 = EditorUtility.CreateGameObjectWithHideFlags("SceneLight", HideFlags.HideAndDontSave, new Type[] { typeof(Light) });
			m_Light[i] = objectWithHideFlags2.GetComponent<Light>();
			m_Light[i].type = LightType.Directional;
			m_Light[i].intensity = 0.5f;
			m_Light[i].enabled = false;
		}
		m_Light[0].color = LevelEditor.kSceneViewFrontLight;
		m_Light[1].color = LevelEditor.kSceneViewUpLight - LevelEditor.kSceneViewMidLight;
		m_Light[1].transform.LookAt(Vector3.down);
		m_Light[1].renderMode = LightRenderMode.ForceVertex;
		m_Light[2].color = LevelEditor.kSceneViewDownLight - LevelEditor.kSceneViewMidLight;
		m_Light[2].transform.LookAt(Vector3.up);
		m_Light[2].renderMode = LightRenderMode.ForceVertex;
		HandleUtility.handleMaterial.SetColor("_SkyColor", LevelEditor.kSceneViewUpLight * 1.5f);
		HandleUtility.handleMaterial.SetColor("_GroundColor", LevelEditor.kSceneViewDownLight * 1.5f);
		HandleUtility.handleMaterial.SetColor("_Color", LevelEditor.kSceneViewFrontLight * 1.5f);
	}

	private void ResetIfNaN()
	{
		if (float.IsInfinity(m_Position.x) || float.IsNaN(m_Position.x))
			m_Position = Vector3.zero;
		if (!float.IsInfinity(m_Rotation.x) && !float.IsNaN(m_Rotation.x))
			return;
		m_Rotation = Quaternion.identity;
	}

	private float GetVerticalFOV(float aspectNeutralFOV)
	{
		return Mathf.Atan(Mathf.Tan(aspectNeutralFOV * 0.5f * Mathf.Deg2Rad) * 0.7071068f / Mathf.Sqrt(m_Camera.aspect)) * 2.0f * Mathf.Rad2Deg;
	}

	private float GetVerticalOrthoSize()
	{
		return m_Size * 0.7071068f / Mathf.Sqrt(m_Camera.aspect);
	}

	private float cameraDistance
	{
		get
		{
			float num = 90; //m_Ortho.Fade(90f, 0.0f);
			if (!m_Camera.orthographic)
				return m_Size / Mathf.Tan(num * 0.5f * Mathf.Deg2Rad);
			else
				return m_Size * 2f;
		}
	}

	private static RenderingPath GetSceneViewRenderingPath()
	{
		if (Camera.main != null)
			return Camera.main.renderingPath;
		Camera[] allCameras = Camera.allCameras;
		if (allCameras != null && allCameras.Length == 1)
			return allCameras[0].renderingPath;
		return RenderingPath.UsePlayerSettings;
	}

	internal static void DrawCameraImpl(Rect position, Camera camera, DrawCameraMode drawMode, bool drawGrid /*, DrawGridParameters gridParam*/, bool finish)
	{
		if (camera.targetTexture == null)
		{
			Rect rect = position; //new Rect(20, 20, 500, 500);//GUIClip.Unclip(position);
			rect.xMin += 3;
			rect.yMin += 17;
			rect.height += 2;

			camera.pixelRect = new Rect(rect.xMin, (float)Screen.height - rect.yMax, rect.width, rect.height);
		}
		else
			camera.rect = new Rect(0.0f, 0.0f, 1f, 1f);
		if (drawMode == DrawCameraMode.Normal)
		{
			RenderTexture targetTexture = camera.targetTexture;
			camera.targetTexture = RenderTexture.active;
			camera.Render();
			camera.targetTexture = targetTexture;
		}
	}

	private bool UpdateCursorBox(Vector2 mousePos)
	{
		Ray ray = HandleUtility.GUIPointToWorldRay(mousePos);
		IntVector3? position = cursorPositionSelector.GetByWorldRay(ray);

//		IntVector3? position = cursorPositionSelector.GetByGUIPoint(mousePos, Camera.current);
//		bool raycast = !clearing && !drawing;
//		bool visible = GUIPointToGridPosition(mousePos, raycast, currentHeightDrawing, out position);
		bool visible = position.HasValue;
		bool changed = visible != _cursorBlockVisible;
		_cursorBlockVisible = visible;

		if (visible)
		{
			changed |= position != _cursorBlockPos;
			_cursorBlockPos = position.Value;
		}

		return changed;
	}

	private IVoxelStorage GetStorage()
	{
		VoxelStorageComponent storageComponent = GetStorageComponent();
		return storageComponent != null ? storageComponent.GetStorage() : null;
	}

	private VoxelStorageComponent GetStorageComponent()
	{
		return storageComponent ?? (storageComponent = FindObjectsOfType<VoxelStorageComponent>().SingleOrDefault());
	}
}
