﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Character))]
public class CharacterEditor : Editor
{
	private List<bool> toggle = new List<bool>();

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		var character = (Character)target;
		var animator = character.GetComponent<Animator>();
		if (animator != null)
		{
			int i = 0;
			foreach (AnimatorClipInfo clipInfo in animator.GetNextAnimatorClipInfo(0))
//			foreach (AnimatorControllerParameter clipInfo in animator.parameters)
//			foreach (AnimatorStateInfo clipInfo in animator.GetNextAnimatorStateInfo(0))
			{
//				if (clipInfo.type == )
				while (toggle.Count < i)
					toggle.Add(false);
				toggle[i] = GUILayout.Toggle(toggle[i], clipInfo.clip.name);
				if (toggle[i])
				{
//					animator.
				}
				i++;
			}
		}

//		if (GUILayout.Toolbar(0, new string[] {"a", "b", "c"}, GUILayout.ExpandWidth(true)) == 0)
		{
//			character.CollectBatches();
		}
//		toggle = GUILayout.Toggle(toggle, "Batch1");
//        if (toggle)
		{
//			character.CollectBatches();
		}
	}
}
