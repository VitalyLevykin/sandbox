﻿public class AnimationNames
{
	public static readonly string IDLE = "Idle";
	public static readonly string WALK = "Walk";
	public static readonly string MELEE_ATTACK = "Attack";
}
