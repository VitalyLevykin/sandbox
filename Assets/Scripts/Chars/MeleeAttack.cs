﻿using UnityEngine;

public class MeleeAttack : BaseBehaviour
{
	public GameObject target;
	public Animator attackAnimator;
	public AnimationClip attackAnimationClip;
	public float rotationSpeed = 180;
	public float moveSpeed = 1;
	public float attackRange = 1;

	bool attacking;
	bool walking;
	float attackBeginTime;

	public void OnEnable()
	{
	}

	public void Update()
	{
		if (attacking)
		{
			if (Time.time >= attackBeginTime + attackAnimationClip.averageDuration)
			{
				attacking = false;
			}
		}
		else
		{
			if (target != null)
			{
				Vector3 targetPosition = target.transform.position;
				Vector3 diffDistance = targetPosition - transform.position;
				float maxMove = moveSpeed * Time.deltaTime;
				float toMove = 0;
				bool endMove = false;
				if (diffDistance.magnitude > maxMove + attackRange)
				{
					toMove = moveSpeed * Time.deltaTime;
				}
				else
				{
					toMove = diffDistance.magnitude - attackRange;
					endMove = true;
				}
//				float diffRotation = Quaternion.LookRotation(targetPosition - transform.position).eulerAngles.y - transform.rotation.eulerAngles.y;
//				float maxRotation = rotationSpeed * Time.deltaTime;
//				float toRotate = 0;
//				bool endRotation = false;
//				if (Mathf.Abs(diffRotation) > maxRotation)
//				{
//					toRotate = maxRotation;
//				}
//				else
//				{
//					toRotate = diffRotation;
//					endRotation = true;
//				}
				// apply
				transform.position += diffDistance.normalized * toMove;
				transform.LookAt(targetPosition);
				transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
//				transform.Rotate(Vector3.down, toRotate);

				if (endMove /*&& endRotation*/)
				{
					Attack();
				}
				bool prevWalking = walking;
				walking = !(endMove /*&& endRotation*/);
				if (walking && !prevWalking)
				{
					attackAnimator.SetBool(AnimationNames.WALK, true);
				}
				else if (!walking && prevWalking)
				{
					attackAnimator.SetBool(AnimationNames.WALK, false);
				}
			}
			else
			{
				enabled = false;
			}
		}
	}

	void Attack()
	{
		attacking = true;
		attackBeginTime = Time.time;
		//attackAnimator.SetTrigger(AnimationNames.MELEE_ATTACK);
		attackAnimator.SetTrigger(AnimationNames.MELEE_ATTACK);
	}
}
