﻿using UnityEngine;

public static class HierarchyUtils
{
	public static GameObject GetOrCreateChild(this Transform transform, string childName)
	{
		Transform childTransform = transform.FindChild(childName);
		if (childTransform == null)
		{
			GameObject child = new GameObject(childName);
			child.transform.parent = transform;
			return child;
		}
		return childTransform.gameObject;
	}
}
