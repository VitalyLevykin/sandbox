﻿using System;
using UnityEngine;
using UnityEngine.Assertions.Must;

public static class GameObjectExt
{
	public static void Event<T>(this GameObject gameObject, Action<T> e) where T : BaseEvent
	{
		// TODO optimize, RequireReceiver mode

		Component[] components = gameObject.GetComponents(typeof(T));
		for (int i = 0; i < components.Length; i++)
		{
			var behaviour = (Behaviour)components[i];
			if (behaviour.enabled)
			{
				var t = (T)(object)components[i];
				e(t);
			}
		}
	}

	public static void TraverseChildrenWithoutSelf(this GameObject gameObject, Action<GameObject> action)
	{
		int childCount = gameObject.transform.childCount;
		for (int i = 0; i < childCount; i++)
		{
			TraverseChildrenWithSelf(gameObject.transform.GetChild(i).gameObject, action);
		}

		childCount.MustBeEqual(gameObject.transform.childCount);
	}

	public static void TraverseChildrenWithSelf(this GameObject gameObject, Action<GameObject> action)
	{
		action(gameObject);

		int childCount = gameObject.transform.childCount;
		for (int i = 0; i < childCount; i++)
		{
			TraverseChildrenWithSelf(gameObject.transform.GetChild(i).gameObject, action);
		}

		childCount.MustBeEqual(gameObject.transform.childCount);
	}
}
