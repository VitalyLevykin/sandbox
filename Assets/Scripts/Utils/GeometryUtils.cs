﻿using UnityEngine;

public static class GeometryUtils
{
	public static bool Raycast(Vector2 mousePos, out RaycastHit hit, Camera camera)
	{
		Ray ray = GUIPointToWorldRay(mousePos, camera);
		return Physics.Raycast(ray, out hit);
	}
	
	public static Vector3 FloorToGrid(Vector3 position, float blockSize)
	{
		return new Vector3(FloorToGrid(position.x, blockSize), FloorToGrid(position.y, blockSize), FloorToGrid(position.z, blockSize));
	}
	
	public static float FloorToGrid(float value, float blockSize)
	{
		float blocks = value/blockSize;
		var ceiledBlocks = (float) Mathf.Floor(blocks);
		return ceiledBlocks*blockSize;
	}

	// decompiled HandleUtility.GUIPointToWorldRay(Vector2)
	public static Ray GUIPointToWorldRay(Vector2 position, Camera camera)
	{
		if (camera == null)
		{
			Debug.LogError((object)"Unable to convert GUI point to world ray if a camera has not been set up!");
			return new Ray(Vector3.zero, Vector3.forward);
		}
		else
		{
			Vector2 vector2 = position;//GUIClip.Unclip(position);
			vector2.y = (float)Screen.height - vector2.y;
			return camera.ScreenPointToRay((Vector3)new Vector2(vector2.x, vector2.y));
		}
	}
}
