﻿using UnityEngine;

public static class MathUtils
{
	public static bool AlmostEqual(float a, float b, float epsilon)
	{
		return Mathf.Abs(a - b) < epsilon;
	}

	public static bool AlmostEqual(Vector2 a, Vector2 b, float epsilon)
	{
		return AlmostEqual(a.x, b.x, epsilon) && AlmostEqual(a.y, b.y, epsilon);
	}
}
