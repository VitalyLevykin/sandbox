﻿using UnityEngine;

public static class InputUtils
{
	public static bool IsEmulatedMouseEvent(Event e)
	{
		return IsMouseEvent(e) && IsTouchEvent();
	}

	public static bool IsMouseEvent(Event e)
	{
		return (e.type == EventType.MouseDown || e.type == EventType.MouseUp || e.type == EventType.MouseDrag || e.type == EventType.MouseDrag || e.type == EventType.MouseMove || e.type == EventType.ScrollWheel);
	}

	public static bool IsTouchEvent()
	{
		return Input.touchCount != 0;
	}

	public static Vector2 TouchSpeed(int touchIndex)
	{
		float deltaTime = Input.GetTouch(touchIndex).deltaTime;
		if (deltaTime < 0.01f)
			deltaTime = 0.01f;
		return Input.GetTouch(touchIndex).deltaPosition / deltaTime;
	}
}
