﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

[TestFixture]
public class QuadTreeTest
{
	private QuadTree<object> quadTree;

	[SetUp]
	public void SetUp()
	{
		quadTree = new QuadTree<object>();
	}

	[TearDown]
	public void TearDown()
	{
		quadTree = null;
	}

	[Test]
	public void TestEnumerator()
	{
		quadTree.root = new QuadTreeNode<object>();
		quadTree.root.m00 = new QuadTreeNode<object>();
		quadTree.root.m10 = new QuadTreeNode<object>();
		quadTree.root.m01 = new QuadTreeNode<object>();
		quadTree.root.m00.m00 = new QuadTreeNode<object>();

		quadTree.root.m00.element = 1;
		quadTree.root.m10.element = 2;
		quadTree.root.m01.element = 3;
		quadTree.root.m00.m00.element = 11;

		Assert.AreEqual(new List<int> { 1, 11, 2, 3 }, quadTree.ToList());
	}

	[Test]
	public void SetGet()
	{
		object prev = quadTree.Set(1, 2, 3);

		Assert.IsNull(prev);
		Assert.AreEqual(quadTree.Get(1, 2), 3);
		Assert.AreEqual(quadTree.Count(), 1);
	}

	[Test]
	public void SetSet()
	{
		quadTree.Set(1, 2, 3);
		object prev = quadTree.Set(1, 2, 3);

		Assert.AreEqual(prev, 3);
		Assert.AreEqual(quadTree.Get(1, 2), 3);
		Assert.AreEqual(quadTree.Count(), 1);
	}

	[Test]
	public void Replace()
	{
		quadTree.Set(1, 2, 3);
		quadTree.Set(1, 2, 4);

		Assert.AreEqual(quadTree.Get(1, 2), 4);
		Assert.AreEqual(quadTree.Count(), 1);
	}

	[Test]
	public void GetEmpty()
	{
		quadTree.Set(0, 1, 1);
		quadTree.Set(1, 0, 2);
		quadTree.Set(0, -1, 3);
		quadTree.Set(-1, 0, 4);

		Assert.IsNull(quadTree.Get(0, 0));
		Assert.AreEqual(quadTree.Count(), 4);
	}

	[Test]
	public void Clear()
	{
		throw new NotImplementedException();
	}

	[Test]
	public void SetLine()
	{
		for (int i = 0; i < 100; i++)
		{
			quadTree.Set(i, i, i*10);
		}

		for (int i = 0; i < 100; i++)
		{
			Assert.AreEqual(quadTree.Get(i, i), i*10);
		}

		Assert.AreEqual(quadTree.Count(), 100);
	}
}
