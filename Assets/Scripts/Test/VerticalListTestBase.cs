﻿using System;
using NUnit.Framework;

[TestFixture]
public class VerticalListTestBase
{
	protected VerticalList<object, VerticalListChunk<object>> list;

	[SetUp]
	public void SetUp()
	{
		list = new VerticalList<object, VerticalListChunk<object>>();
	}

	[TearDown]
	public void TearDown()
	{
		ValidateList(list);
		list = null;
	}

	private void ValidateList<T>(VerticalList<T, VerticalListChunk<T>> list)
	{
		if (list.IsEmpty())
			return;

		var chunks = list.GetChunks();
		T prevElement = default(T);
		int prevBottom = int.MaxValue;
		foreach (var chunk in chunks)
		{
			try
			{
				chunk.CheckState();
			}
			catch (ArgumentException e)
			{
				//Console.WriteLine(e);
				ReportInvalidList(e.ToString());
			}
			if (chunk.Top >= prevBottom)
				ReportInvalidList("Overlap");
			if ((chunk.Top == prevBottom - 1) && chunk.Element.Equals(prevElement))
				ReportInvalidList("Equal elements connected");

			prevElement = chunk.Element;
			prevBottom = chunk.Bottom;
		}
	}

	private void ReportInvalidList(string message)
	{
		list.Dump();
		//Console.WriteLine("Invalid list state: " + message);
		throw new Exception("Invalid list state: " + message);
	}
}
