﻿using NUnit.Framework;

[TestFixture]
public class ClearTest : VerticalListTestBase
{
	[Test]
	public void SetDefaultOneElement()
	{
		list.Set(10, 100);
		list.ClearElement(10);
		Assert.IsNull(list.Get(10));
		Assert.IsTrue(list.IsEmpty());
	}

	[Test]
	public void SetDefaultTopElement()
	{
		list.Set(12, 100);
		list.Set(11, 100);
		list.Set(10, 100);

		list.ClearElement(12);

		Assert.IsNull(list.Get(13));
		Assert.IsNull(list.Get(12));
		Assert.AreEqual(100, list.Get(11));
		Assert.AreEqual(100, list.Get(10));
		Assert.IsNull(list.Get(9));
		Assert.AreEqual(1, list.GetChunks().Count);
	}

	[Test]
	public void SetDefaultBottomElement()
	{
		list.Set(12, 100);
		list.Set(11, 100);
		list.Set(10, 100);

		list.ClearElement(10);

		Assert.IsNull(list.Get(13));
		Assert.AreEqual(100, list.Get(12));
		Assert.AreEqual(100, list.Get(11));
		Assert.IsNull(list.Get(10));
		Assert.IsNull(list.Get(9));
		Assert.AreEqual(1, list.GetChunks().Count);
	}

	[Test]
	public void SetDefaultMiddleElement()
	{
		list.Set(12, 100);
		list.Set(11, 100);
		list.Set(10, 100);

		list.ClearElement(11);

		Assert.IsNull(list.Get(13));
		Assert.AreEqual(100, list.Get(12));
		Assert.IsNull(list.Get(11));
		Assert.AreEqual(100, list.Get(10));
		Assert.IsNull(list.Get(9));
		Assert.AreEqual(2, list.GetChunks().Count);
	}
}
