﻿using NUnit.Framework;
using System;

[TestFixture]
public class SetTest : VerticalListTestBase
{
	[Test]
	public void EmptyList()
	{
		Assert.IsNull(list.Get(0));
		Assert.IsNull(list.Get(10));
		Assert.IsNull(list.Get(0));
	}

	[Test]
	[ExpectedException(typeof (ArgumentOutOfRangeException))]
	public void GetStrict()
	{
		list.GetStrict(2);
	}

	[Test]
	public void AddNewEvent()
	{
		object o0 = null;
		object o1 = null;
		list.onAddNew += (position, chunk) =>  o0 = list.Get(0);
		list.onAddNew += (position, chunk) =>  o1 = list.Get(1);
		list.Set(0, new object());
		Assert.IsNotNull(o0);
		Assert.IsNull(o1);
	}

	[Test]
	public void SetToEmpty()
	{
		var obj = new object();
		list.Set(0, obj);
		Assert.IsTrue(list.Has(0));
		Assert.AreEqual(obj, list.Get(0));
	}

	[Test]
	public void SetTwice()
	{
		list.Set(1, 10);
		var prev = list.Set(1, 20);
		Assert.IsTrue(list.Has(1));
		Assert.AreEqual(20, list.Get(1));
		Assert.AreEqual(10, prev);
	}

	[Test, Sequential]
	public void SetSparse(
		[Values(0, 0, 2, 2)] int index1,
		[Values(2, 2, 0, 0)] int index2,
		[Values(1, 1, 1, 1)] int value1,
		[Values(1, 2, 1, 2)] int value2)
	{
		var prev1 = list.Set(index1, value1);
		var prev2 = list.Set(index2, value2);

		Assert.IsNull(prev1);
		Assert.IsNull(prev2);

		Assert.AreEqual(value1, list.Get(index1));
		Assert.AreEqual(value2, list.Get(index2));
		Assert.IsNull(list.Get(1));
	}

	[Test, Sequential]
	public void SetJustHigher(
		[Values(1, 1, 1, 2, 1)] int valueTop,
		[Values(1, 1, 2, 1, 2)] int valueBottom,
		[Values(1, 2, 2, 2, 3)] int valueInsert,
		[Values(2, 3, 2, 3, 3)] int chunks)
	{
		list.Set(13, valueTop);
		list.Set(10, valueBottom);

		var prev = list.Set(11, valueInsert);
		Assert.IsNull(prev);

		Assert.IsNull(list.Get(14));
		Assert.AreEqual(valueTop, list.Get(13));
		Assert.IsNull(list.Get(12));
		Assert.AreEqual(valueInsert, list.Get(11));
		Assert.AreEqual(valueBottom, list.Get(10));
		Assert.IsNull(list.Get(9));
		Assert.AreEqual(chunks, list.GetChunks().Count);
	}

	[Test, Sequential]
	public void SetJustLower(
		[Values(1, 1, 1, 2, 1)] int valueTop,
		[Values(1, 1, 2, 1, 2)] int valueBottom,
		[Values(1, 2, 2, 2, 3)] int valueInsert,
		[Values(2, 3, 3, 2, 3)] int chunks)
	{
		list.Set(13, valueTop);
		list.Set(10, valueBottom);

		var prev = list.Set(12, valueInsert);
		Assert.IsNull(prev);

		Assert.IsNull(list.Get(14));
		Assert.AreEqual(valueTop, list.Get(13));
		Assert.AreEqual(valueInsert, list.Get(12));
		Assert.IsNull(list.Get(11));
		Assert.AreEqual(valueBottom, list.Get(10));
		Assert.IsNull(list.Get(9));
		Assert.AreEqual(chunks, list.GetChunks().Count);
	}

	[Test, Sequential]
	public void SetBetween(
		[Values(1, 1, 1, 2, 1)] int valueTop,
		[Values(1, 1, 2, 1, 2)] int valueBottom,
		[Values(1, 2, 2, 2, 3)] int valueInsert,
		[Values(1, 3, 2, 2, 3)] int chunks)
	{
		list.Set(12, valueTop);
		list.Set(13, valueTop);
		list.Set(9, valueBottom);
		list.Set(10, valueBottom);

		var prev = list.Set(11, valueInsert);
		Assert.IsNull(prev);

		Assert.IsNull(list.Get(14));
		Assert.AreEqual(valueTop, list.Get(13));
		Assert.AreEqual(valueTop, list.Get(12));
		Assert.AreEqual(valueInsert, list.Get(11));
		Assert.AreEqual(valueBottom, list.Get(10));
		Assert.AreEqual(valueBottom, list.Get(9));
		Assert.IsNull(list.Get(8));
		Assert.AreEqual(chunks, list.GetChunks().Count);
	}

	[Test, Sequential]
	public void SetBetweenSparse(
		[Values(1, 1, 1, 2, 1)] int valueTop,
		[Values(1, 1, 2, 1, 2)] int valueBottom,
		[Values(1, 2, 2, 2, 3)] int valueInsert)
	{
		list.Set(14, valueTop);
		list.Set(10, valueBottom);

		var prev = list.Set(12, valueInsert);
		Assert.IsNull(prev);

		Assert.AreEqual(valueTop, list.Get(14));
		Assert.IsNull(list.Get(13));
		Assert.AreEqual(valueInsert, list.Get(12));
		Assert.IsNull(list.Get(11));
		Assert.AreEqual(valueBottom, list.Get(10));
		Assert.AreEqual(3, list.GetChunks().Count);
	}

	// TODO set inside tests
	// TODO set null tests

	[Test]
	public void MergeWithReplace()
	{
		list.Set(1, 1);
		list.Set(2, 2);
		list.Set(3, 1);

		var prev = list.Set(2, 1);
		Assert.AreEqual(2, prev);

		Assert.IsNull(list.Get(0));
		Assert.AreEqual(1, list.Get(1));
		Assert.AreEqual(1, list.Get(2));
		Assert.AreEqual(1, list.Get(3));
		Assert.IsNull(list.Get(4));
		Assert.AreEqual(1, list.GetChunks().Count);
	}

	[Test]
	public void ReplaceReduceAndNew()
	{
		list.Set(3, 1);
		list.Set(2, 2);
		list.Set(1, 2);

		list.Set(2, 3);

		Assert.IsNull(list.Get(0));
		Assert.AreEqual(1, list.Get(3));
		Assert.AreEqual(3, list.Get(2));
		Assert.AreEqual(2, list.Get(1));
		Assert.IsNull(list.Get(4));
		Assert.AreEqual(3, list.GetChunks().Count);
		list.Dump();
	}

	[Test]
	public void ReplaceReduceAndAppend()
	{
		list.Set(3, 1);
		list.Set(2, 2);
		list.Set(1, 2);

		list.Set(2, 1);

		Assert.IsNull(list.Get(0));
		Assert.AreEqual(1, list.Get(3));
		Assert.AreEqual(1, list.Get(2));
		Assert.AreEqual(2, list.Get(1));
		Assert.IsNull(list.Get(4));
		Assert.AreEqual(2, list.GetChunks().Count);
		list.Dump();
	}

	[Test]
	public void ExpandDown1()
	{
		list.Set(5, 1);
		list.Set(4, 2);

		var prev = list.Set(4, 1);
		Assert.AreEqual(2, prev);

		Assert.AreEqual(1, list.Get(5));
		Assert.AreEqual(1, list.Get(4));
		Assert.AreEqual(1, list.GetChunks().Count);
	}

	[Test]
	public void ExpandDown()
	{
		list.Set(5, 1);
		list.Set(4, 2);
		list.Set(3, 2);

		var prev = list.Set(4, 1);
		Assert.AreEqual(2, prev);

		Assert.AreEqual(1, list.Get(5));
		Assert.AreEqual(1, list.Get(4));
		Assert.AreEqual(2, list.Get(3));
		Assert.AreEqual(2, list.GetChunks().Count);
	}

	[Test]
	public void ExpandDown222222()
	{
		list.Set(5, 1);
		list.Set(4, 2);
		list.Set(3, 2);

		Assert.AreEqual(1, list.Get(5));
		Assert.AreEqual(2, list.Get(4));
		Assert.AreEqual(2, list.Get(3));
		Assert.AreEqual(2, list.GetChunks().Count);
	}

	[Test]
	public void ExpandDown3333()
	{
		list.Set(5, 1);
		list.Set(4, 2);

		Assert.AreEqual(1, list.Get(5));
		Assert.AreEqual(2, list.Get(4));
		Assert.AreEqual(2, list.GetChunks().Count);
	}
}
