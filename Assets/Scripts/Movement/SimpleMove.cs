﻿using UnityEngine;

public class SimpleMove : BaseBehaviour
{
	public float speed = 1;
	private bool go;
	private Vector3 targetPosition;
	public float maxClickDuration = 0.3f;
	private float clickBeginTime;
	private Vector2 clickScreenPosition;
	private IVoxelStorage voxelStorage;
	private SceneBuilder sceneBuilder;

	void Awake()
	{
		voxelStorage = FindObjectOfType<VoxelStorageComponent>().GetStorage();
		sceneBuilder = FindObjectOfType<SceneBuilder>();
	}

	void OnGUI()
	{
		Event e = Event.current;

		if (!InputUtils.IsMouseEvent(e) && !InputUtils.IsTouchEvent())
			return;

		if (InputUtils.IsEmulatedMouseEvent(e))
			return;

		Vector2 clickPosition = Input.mousePosition;//InputUtils.IsMouseEvent(e) ? new Vector2(e.mousePosition.x, Screen.height - e.mousePosition.y) : new Vector2(Input.GetTouch(0).position.x,/* Screen.height -*/ Input.GetTouch(0).position.y);

		if (e.type == EventType.MouseDown && e.button == 0 || (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began))
		{
			clickBeginTime = Time.time;
			clickScreenPosition = clickPosition;
		}
		if (Input.touchCount >= 2)
		{
			clickBeginTime = 0;
		}
		if ((e.type == EventType.MouseUp && e.button == 0) || (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended))
		{
			if (MathUtils.AlmostEqual(clickScreenPosition, clickPosition, 10) && Time.time - clickBeginTime < maxClickDuration)
			{
				clickBeginTime = 0;
				Ray ray = Camera.main.ScreenPointToRay(clickPosition);
				IntVector3 insidePosition, outsidePosition;
				bool hasHit = CursorPositionSelector.RaycastToInt(ray, out insidePosition, out outsidePosition);
				if (hasHit)
				{
					//					MapVoxel insideVoxel = voxelStorage.Get(insidePosition);
					//					GameObject targetObject = insideVoxel.Data.storedObject;
					GameObject targetObject = sceneBuilder.GetViewObjectAt(insidePosition);
					if (targetObject != null && targetObject.GetComponent<EnemyComponent>() != null)
					{
						Debug.Log("ATTACK!");
						//						targetObject.GetComponent<Animator>().Play("Attack");
						//						targetObject.GetComponent<Animator>().SetTrigger("Attack");

						var meleeAttack = targetObject.GetComponent<MeleeAttack>();
						if (meleeAttack != null)
						{
							meleeAttack.enabled = true;
							meleeAttack.target = gameObject;
						}
					}
					else
					{
						Debug.Log("Go to " + outsidePosition);
						go = true;
						targetPosition = MapPositionConverter.ToFloat(outsidePosition) + new Vector3(0.5f, 0.0f, 0.5f);
						Vector3 posCenterDown = targetPosition + Vector3.up * 0.001f;
						Debug.DrawLine(posCenterDown + new Vector3(-0.2f, 0, -0.2f), posCenterDown + new Vector3(0.2f, 0, 0.2f), Color.red, 0.5f);
						Debug.DrawLine(posCenterDown + new Vector3(-0.2f, 0, 0.2f), posCenterDown + new Vector3(0.2f, 0, -0.2f), Color.red, 0.5f);
						transform.LookAt(targetPosition);
						transform.localEulerAngles = Vector3.Scale(transform.localEulerAngles, new Vector3(0, 1, 1));
						GetComponent<Animator>().Play(AnimationNames.WALK);
					}
				}
			}
		}
	}

	void Update()
	{
		if (go)
		{
			Vector3 path = targetPosition - transform.position;
			float distanceSqr = Vector3.SqrMagnitude(path);
			float delta = speed * Time.deltaTime;
			if (distanceSqr < delta * delta)
			{
				transform.position = targetPosition;
				go = false;
				GetComponent<Animator>().Play(AnimationNames.IDLE);
			}
			else
			{
				transform.position += path.normalized * delta;
			}
		}
	}
}
