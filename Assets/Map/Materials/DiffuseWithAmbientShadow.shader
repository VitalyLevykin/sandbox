﻿Shader "Custom/DiffuseWithAmbientShadow"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_ShadowColor ("Shadow Color", Color) = (0,0,0,1)
		_AmbientShadowColor ("Ambient Shadow Color", Color) = (0,0,0,1)
		_AmbientShadingStrength ("Ambient Shading Strength", float) = 0.5
	}

	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 150

		CGPROGRAM
		#pragma surface surf CSLambert vertex:vert

		sampler2D _MainTex;
		fixed4 _ShadowColor;
		fixed4 _AmbientShadowColor;
		fixed _AmbientShadingStrength;

		struct Input
		{
			float2 uv_MainTex;
			float3 vertColor;
		};

		void vert (inout appdata_full v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.vertColor = lerp(v.color, _AmbientShadowColor, (1 - v.color.a) * _AmbientShadingStrength);
		}

		void surf (Input IN, inout SurfaceOutput o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb * IN.vertColor;
			o.Alpha = c.a;
		}

		half4 LightingCSLambert (SurfaceOutput s, half3 lightDir, half atten) 
		{
			fixed diff = max (0, dot (s.Normal, lightDir));

			fixed4 c;
			c.rgb = s.Albedo * _LightColor0.rgb * (diff * atten);
			
			//shadow colorization
			c.rgb += _ShadowColor.xyz * max(0.0, (1.0 - (diff * atten)));
			c.a = s.Alpha;
			return c;
		}
		ENDCG
	}

	Fallback "Diffuse"
}
